# Profitable opportunities in DEFI
This learning project includes arbitrage and liquidation bots, most part of the project belongs to the liquidator module where I worked the most and was able to capture profit in the past. It is old now and there are a lot of improvements that we can do, as it was one of my first developments in DEFI.

## About The Project
This code is full of experiments and learning. Do not try to use any of the modules inside just as is as you might lose money in the process.

### Liquidations
The code for liquidations works for most compound protocol forks on EVM compatible networks, it allows the usage of a flash loan to obtain the funds for the liquidation, doing atomic swaps and using 1inch router aggregation protocol API and deployed smart contracts to swap between required assets in the fly.

