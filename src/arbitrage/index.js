require('dotenv').config({ path: '../../.env' });
const ethers = require('ethers');
const { isArray } = require('lodash');
const { BigNumber } = require('bignumber.js');
const { abi: IUniswapV2Pair } = require('@uniswap/v2-periphery/build/IUniswapV2Pair.json');
const { utils: { hexDataSlice, defaultAbiCoder, formatEther, formatUnits } } = ethers;
const { setupMempoolInterval } = require('../common/mempool');
const { EXCHANGES: { PANCAKE } } = require('./dex');
const { arbRoutes, tokens, arbitragePairs } = require('./arbPairs.json');
const { BSC_PROVIDER, LOCAL_PROVIDER } = process.env;

BigNumber.set({ DECIMAL_PLACES: 18 });

const SIGNATURES = {
  swapETHForExactTokens: '0xfb3bdb41',
  swapExactETHForTokens: '0x7ff36ab5',
  swapExactTokensForETH: '0x18cbafe5',
  swapExactTokensForTokens: '0x38ed1739',
  swapTokensForExactETH: '0x4a25d94a',
  swapTokensForExactTokens: '0x8803dbee',
}

const decodeBySignature = {
  "0xfb3bdb41": (data) => defaultAbiCoder.decode(['uint256', 'address[]', 'address', 'uint256'], data),
  "0x7ff36ab5": (data) => defaultAbiCoder.decode(['uint256', 'address[]', 'address', 'uint256'], data),
  "0x18cbafe5": (data) => defaultAbiCoder.decode(['uint256', 'uint256', 'address[]', 'address', 'uint256'], data),
  "0x38ed1739": (data) => defaultAbiCoder.decode(['uint256', 'uint256', 'address[]', 'address', 'uint256'], data),
  "0x4a25d94a": (data) => defaultAbiCoder.decode(['uint256', 'uint256', 'address[]', 'address', 'uint256'], data),
  "0x8803dbee": (data) => defaultAbiCoder.decode(['uint256', 'uint256', 'address[]', 'address', 'uint256'], data),
}

const SIGNATURES_VALUES = Object.values(SIGNATURES);

const TOKEN_SYMBOL_BY_ADDRESS_MAP = Object.keys(tokens).reduce((acc, symbol) => ({
  ...acc,
  [tokens[symbol].toLowerCase()]: symbol,
}), {});

let GAS_PRICES = {};
const RESERVES = {};

const mempoolHandler = (pendingTxs) => {
  // Reset previous TXs
  GAS_PRICES = {};

  // Fetch current pending
  pendingTxs.forEach(processDexPendingTxs);

  if (Object.keys(RESERVES).length < Object.keys(arbitragePairs).length) return;

  // Run Arbitrage!
  arbitrage();
};

const processDexPendingTxs = ([pendingTx]) => {
  const method = hexDataSlice(pendingTx.input, 0, 4);
  if (SIGNATURES_VALUES.includes(method)) {
    const data = decodeBySignature[method](hexDataSlice(pendingTx.input, 4));
    const path = isArray(data[1]) ? data[1] : data[2];
    const tokenIn = TOKEN_SYMBOL_BY_ADDRESS_MAP[path[0].toLowerCase()];
    const tokenOut = TOKEN_SYMBOL_BY_ADDRESS_MAP[path[path.length - 1].toLowerCase()];

    if (!tokenIn || !tokenOut) {
      return;
    }

    const gasPrice = new BigNumber(pendingTx.gasPrice);
    const pairName = Object.keys(arbitragePairs).find(pKey => pKey.includes(tokenIn) && pKey.includes(tokenOut));

    if (!pairName) return;

    // GasPrice is lower than the previous one.
    if (!!GAS_PRICES[pairName] && GAS_PRICES[pairName].gasPrice.gt(gasPrice)) return;

    // console.log(`${pairName}: Gas price: ${formatUnits(gasPrice.toString(), 'gwei')} gwei`);

    GAS_PRICES[pairName] = {
      hash: pendingTx.hash,
      gasLimit: parseInt(pendingTx.gas),
      gasPrice,
      timestamp: new Date().getTime()
    };
  }
}

function getAmountOut(reserveA, reserveB, amountIn) {
  const amountInWithFee = amountIn.times(9975); // 0.25% fees
  const numerator = amountInWithFee.times(reserveB);
  const denominator = reserveA.times(10000).plus(amountInWithFee);
  const amountOut = numerator.div(denominator);
  return amountOut;
}

function parseArb({pairName, tokenOut, tokenIn, slippage = 0.3 }, amountIn) {  
  const reserve0 = RESERVES[pairName][tokenIn];
  const reserve1 = RESERVES[pairName][tokenOut];

  let amountOut = getAmountOut(reserve0, reserve1, amountIn);

  const calculatedSlippage = 10000 - (slippage*100);

  // Add 0.3% slippage by default
  amountOut = amountOut.times(calculatedSlippage).div(10000);

  return amountOut;
}

function convertBUSDtoWBNB(amountIn) {
  const busdReserves = RESERVES['WBNB_BUSD'].BUSD;
  const wbnbReserves = RESERVES['WBNB_BUSD'].WBNB;

  return getAmountOut(busdReserves, wbnbReserves, amountIn);
}

function getCurrentGasData(arbRoute) {
  // find base gas
  const currGasPrice = BigNumber.max(
    !!GAS_PRICES[arbRoute[0].pairName] ? GAS_PRICES[arbRoute[0].pairName].gasPrice : '5000000000',
    !!GAS_PRICES[arbRoute[1].pairName] ? GAS_PRICES[arbRoute[1].pairName].gasPrice : '5000000000',
    !!GAS_PRICES[arbRoute[2].pairName] ? GAS_PRICES[arbRoute[2].pairName].gasPrice : '5000000000',
  );
  const defaultGas = { gasLimit: 350000, gasPrice: '5000000000' };
  return Object.values(GAS_PRICES).find(gasData => gasData.gasPrice.eq(currGasPrice)) || defaultGas;
}

function arbitrage() {
  const results = [];
  for (arbRoute of arbRoutes) {
    const tokenA = arbRoute[0].tokenIn;
    const reservesBUSD = RESERVES[arbRoute[1].pairName].BUSD;
    const reservesWBNB = RESERVES[arbRoute[0].pairName].WBNB;

    // discard route
    if (reservesBUSD.lt(50000) && reservesWBNB.lt(120)) {
      return;
    }

    const maximumReserves = BigNumber.min(convertBUSDtoWBNB(reservesBUSD), reservesWBNB);

    // Work with an amount of 0.01% of all the reserves.
    const pct = 0.01;
    const amount = maximumReserves.times(10000-(10000-(pct*100))).div(10000);

    const route = [tokenA];
    const amounts = [amount];

    const amount1 = parseArb(arbRoute[0], amount);
    const amount2 = parseArb(arbRoute[1], amount1);
    const amount3 = parseArb(arbRoute[2], amount2);

    route.push(arbRoute[0].tokenOut);
    route.push(arbRoute[1].tokenOut);
    route.push(arbRoute[2].tokenOut);

    amounts.push(amount1);
    amounts.push(amount2);
    amounts.push(amount3);

    const finalAmount = amounts[amounts.length-1];
    const profit = finalAmount.times(9975).div(10000).minus(amount);

    const gasData = getCurrentGasData(arbRoute);
    const maxGasPriceProfitable = profit.div(gasData.gasLimit);
    const diff = maxGasPriceProfitable.minus(gasData.gasPrice / (10**18));
    const outBid = (gasData.gasPrice / (10**18) + (diff * 0.01));

    /*
      try to mine!

      take current gas price, calculate mas gas price profitable and diff

      start with calculating 0.01 % more to the current gas price to win 

      await sendTransaction

      receive tx.hash

      now keep checking MAX GAS prices for the three arbRoutes.pairNames 

      and check that currGasPrice for these swaps are not higher than the Gas price we are using for our tx.

      check highest gas max price as well maybe other Arb has a higher gas than mine check if using above that 
      gas price we still have profits then using it.

      in case it is higher.

        take that new curr gas price and again calculate maxGasProfitable, and diff

        play on if diff still greater than zero (not losing)

        in that case again apply 0.01 over that new curr gas price and send a new TX same nonce but Higher Gas price.

      In case we are losing: our smart contracts gets canceled by default by using our minAmounts etc

      keep doing this until transaction gets mined.
    */

    // Arbitrage opportunity
    if (profit.gt(0)) {
      console.log(`\nFinal amount: ${finalAmount.toString()} ${tokenA} profit: ${profit.toString()}`);
      console.log(`maxGasPriceProfitable: ${maxGasPriceProfitable.toString() * (10**9)} gwei`);
      console.log(`current GasPrice: ${gasData.gasPrice / (10**9)} gwei`);
      console.log(`outBid Gasprice: ${outBid * (10**9)} gwei`);
      console.log(`Amounts: ${amounts.map(amount => amount.toString())}`);
      console.log(`Route: ${JSON.stringify(route)}`);
      console.log(`Gas data: gasPrice:${gasData.gasPrice / (10**9)} - gasLimit:${gasData.gasLimit} - hash: ${gasData.hash}`);
    }
  };

}

function printReserves() {
  Object.keys(RESERVES).forEach((arbKey) => {
    const parts = arbKey.split('_');
    console.log(`${arbKey}: ${RESERVES[arbKey][parts[1]].toString()} ${parts[1]} ${RESERVES[arbKey][parts[2]].toString()} ${parts[2]}`);
  });
}

// setupMempoolInterval(mempoolHandler, [PANCAKE.ROUTER_ADDRESS]);
setupReservesListener();
