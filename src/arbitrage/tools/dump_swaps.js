require('dotenv').config({ path: '../../.env' });
const ethers = require('ethers');
const sqlite3 = require('sqlite3');
const { open } = require('sqlite');
const { LOCAL_PROVIDER } = process.env;
const bscProvider = new ethers.providers.JsonRpcProvider(LOCAL_PROVIDER);

const PAIRS_MAP = {
  WBNB_CAKE_PAIR: '0x0eD7e52944161450477ee417DE9Cd3a859b14fD0',
  WBNB_BUSD_PAIR: '0x58F876857a02D6762E0101bb5C46A8c1ED44Dc16',
  BUSD_USDT_PAIR: '0x7EFaEf62fDdCCa950418312c6C91Aef321375A00',
}

// Setup DATABASE
let db;
(async () => {
  db = await open({
    filename: 'data.sqlite',
    driver: sqlite3.Database
  });
  await db.exec('CREATE TABLE IF NOT EXISTS SWAPS_TX (id INTEGER PRIMARY KEY AUTOINCREMENT, created_at DEFAULT CURRENT_TIMESTAMP, pair_address text, pair_name text, block_number INTEGER, transaction_hash text)');
})();

const PAIR_ABI = [
  "event Swap(address indexed sender, uint amount0In, uint amount1In, uint amount0Out, uint amount1Out, address indexed to)",
];

const pairs = Object.keys(PAIRS_MAP);

async function init() {
  const blockNumber = await bscProvider.getBlockNumber();
  const START_BLOCK = blockNumber-15000;
  console.log(`Working since: ${START_BLOCK} until ${blockNumber}`);

  for (pair of pairs) {
    const contract = new ethers.Contract(PAIRS_MAP[pair], PAIR_ABI, bscProvider);
    const filter = contract.filters.Swap(null);
    let prevFailingIndex = null;
    let retries = 0;

    for (let i = START_BLOCK; i < blockNumber; i+=2000) {
      if (!!prevFailingIndex) {
        i = prevFailingIndex;
      }
      try {
        const events = await contract.queryFilter(filter, i, i+2000);

        await Promise.all(events.map((evt) => {
          return db.run(
            'INSERT INTO SWAPS_TX (pair_address,pair_name,block_number,transaction_hash) VALUES(?,?,?,?)',
            PAIRS_MAP[pair],pair,evt.blockNumber,evt.transactionHash
          );
        }));
        console.log(events.length);
        prevFailingIndex = null;
        retries = 0;
      } catch (err) {
        // Wait 30 secs + some amount before trying next call
        await new Promise(r => setTimeout(r, 30000 + (retries*10000)));
        prevFailingIndex = i;
        retries += 1;
      }
    }
  }
}

init();
