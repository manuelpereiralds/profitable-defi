const fs = require('fs');

const WBNB = '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c';
const BUSD = '0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56';

const FINAL_PAIR = { tokenIn: 'BUSD', tokenOut: 'WBNB', pairName: 'WBNB_BUSD' };

const only_tokens = ['0x0E09FaBB73Bd3Ade0a17ECC321fD13a19e81cE82', '0x8F0528cE5eF7B51152A59745bEfDD91D97091d2F', '0x22168882276e5D5e1da694343b41DD7726eeb288', '0x2f619a1270226fD8ed3Df82f0C1e0cd90804346d', '0xF218184Af829Cf2b0019F8E6F0b2423498a36983', '0x21adB1c644663069e83059AC3f9d9Ca1133D29e4']

const generateArbitragePairsSetup = (pairs) => {
  const availableRoutes = [];
  const addedPairs = [];
  
  const keys = Object.keys(pairs);
  keys.filter(key => only_tokens.some(token => key.includes(token)))
    .filter(key => (key.includes(WBNB) || key.includes(BUSD)))
    .forEach(key => {
      const address = key.split('_').filter(k => k !== WBNB && k !== BUSD)[0];
      if (key.includes(WBNB)) {
        const index = keys.findIndex(k => k === `${address}_${BUSD}` || k === `${BUSD}_${address}`);
        if (index > -1 && !addedPairs.includes(pairs[key].pair_address) && !addedPairs.includes(pairs[keys[index]].pair_address)) {
          availableRoutes.push([pairs[key], pairs[keys[index]]]);
          addedPairs.push(pairs[key].pair_address);
          addedPairs.push(pairs[keys[index]].pair_address);
        }
      }
      if (key.includes(BUSD)) {
        const index = keys.findIndex(k => k === `${address}_${WBNB}` || k === `${WBNB}_${address}`);
        if (index > -1 && !addedPairs.includes(pairs[key].pair_address) && !addedPairs.includes(pairs[keys[index]].pair_address)) {
          availableRoutes.push([pairs[key], pairs[keys[index]]]);
          addedPairs.push(pairs[key].pair_address);
          addedPairs.push(pairs[keys[index]].pair_address);
        }
      }
    });
  
  const finalPairs = { 'WBNB_BUSD': '0x58F876857a02D6762E0101bb5C46A8c1ED44Dc16' };
  const tokens = { WBNB };
  const routes = [];
  
  const getPairName = (pairData) => {
    return `${pairData.base_symbol}_${pairData.quote_symbol}`;
  }
  
  availableRoutes.forEach(data => {
    data.forEach(pairData => {
      finalPairs[getPairName(pairData)] = pairData.pair_address;
      tokens[pairData.base_symbol] = pairData.base_address;
      tokens[pairData.quote_symbol] = pairData.quote_address;
    });

    const route = [];
  
    const pair1 = data[0].base_symbol === 'WBNB' || data[0].quote_symbol === 'WBNB' ? data[0] : data[1];
    const pair2 = data[0].base_symbol === 'BUSD' || data[0].quote_symbol === 'BUSD' ? data[0] : data[1];
  
    const token1 = pair1.base_symbol === 'WBNB' ? pair1.quote_symbol : pair1.base_symbol;
    const token2 = pair2.base_symbol === 'BUSD' ? pair2.base_symbol : pair2.quote_symbol;
  
    route.push({ pairName: getPairName(pair1), tokenIn: 'WBNB', tokenOut: token1 });
    route.push({ pairName: getPairName(pair2), tokenIn: token1, tokenOut: token2 });
    route.push(FINAL_PAIR);
  
    routes.push(route);
  });

  return [finalPairs, tokens, routes];
}

function run() {
  const args = process.argv.slice(2);
  const filename = args[0];
  const { data: pairs } = require(`./${filename}`);

  const [arbitragePairs, tokens, arbRoutes] = generateArbitragePairsSetup(pairs);
  fs.writeFileSync('../arbPairs.json', JSON.stringify({ arbitragePairs, tokens, arbRoutes }));    
}

run();
