require('dotenv').config({ path: '../../.env' });
const ethers = require('ethers');
const sqlite3 = require('sqlite3');
const { open } = require('sqlite');
const { utils: { Interface } } = ethers;
const { LOCAL_PROVIDER } = process.env;
const { data: pairsData } = require('./pairs.json');
const bscProvider = new ethers.providers.JsonRpcProvider(LOCAL_PROVIDER);

// Lookup helpers!
const TOKENS_BY_PAIR = Object.keys(pairsData).reduce((acc, next) => ({ ...acc, [pairsData[next].pair_address]: next }), {});
const TOKENS_BY_ADDRESS = Object.keys(pairsData).reduce((acc, next) => ({
  ...acc,
  [pairsData[next].base_address]: pairsData[next].base_symbol,
  [pairsData[next].quote_address]: pairsData[next].quote_symbol,
}), {});

const getToken = (pairAddress, amount0, amount1) => {
  const tokens = (TOKENS_BY_PAIR[pairAddress] || 'unknown_unknown').split('_');
  const token0 = tokens[0];
  const token1 = tokens[1];
  if (amount0.eq(0)) return [token1, amount1]; 
  if (amount1.eq(0)) return [token0, amount0];
  return ['unknown'];
};

// Setup DATABASE
let db;
(async () => {
  db = await open({
    filename: 'data.sqlite',
    driver: sqlite3.Database
  });
  await db.exec('CREATE TABLE IF NOT EXISTS ARBS (id INTEGER PRIMARY KEY AUTOINCREMENT, pairs text, base text, route text, amount_in text, amount_out text, profit FLOAT, block_number INTEGER, transaction_hash text)');
})().then(init);

const PAIR_ABI = [
  "event Swap(address indexed sender, uint amount0In, uint amount1In, uint amount0Out, uint amount1Out, address indexed to)",
];

async function init() {
  const abi = new Interface(PAIR_ABI);
  const swapsDB = await db.all('SELECT transaction_hash, block_number FROM SWAPS_TX');

  for (swap of swapsDB) {
    const { transaction_hash, block_number } = swap;

    const receipt = await bscProvider.getTransactionReceipt(transaction_hash);  
    const topic = abi.getEventTopic('Swap');
  
    const swapEventLogs = receipt.logs.filter(log => log.topics[0] === topic);

    if (swapEventLogs.length < 3) {
      continue;
    }

    const route = [];

    const LENGTH = swapEventLogs.length;
    let initialAmount;
    let initialToken;
    let lastAmount;

    for (let i = 0; i < LENGTH; i++) {
      const log = swapEventLogs[i];
      const pairAddress = log.address;
      const arguments = abi.parseLog(log).args.slice(1, 5);
      const [tokenIn, tokenInAmount] = getToken(pairAddress, arguments[0], arguments[1]);
      const [tokenOut, tokenOutAmount] = getToken(pairAddress, arguments[2], arguments[3]);

      if (tokenIn === 'unknown') break;
      if (!!lastAmount && !tokenInAmount.eq(lastAmount)) break;

      lastAmount = tokenOutAmount;

      if (i < 1) {
        initialAmount = tokenInAmount;
        initialToken = tokenIn;
      }

      route.push(tokenOut);
    }

    // Cyclic arbitrage found!
    if (!!initialToken && !!lastAmount && initialToken === route[LENGTH-1]) {
      const pairAddresses = swapEventLogs.map((log) => log.address);
      const profit = ((parseInt(lastAmount.toString())/parseInt(initialAmount.toString()))-1 ) * 100;

      await db.run(
        'INSERT INTO ARBS (pairs,base,route,amount_in,amount_out,profit,block_number,transaction_hash) VALUES(?,?,?,?,?,?,?,?)',
        pairAddresses.join(','),
        initialToken,
        route.join(','),
        initialAmount.toString(),
        lastAmount.toString(),
        profit,
        block_number,
        transaction_hash,
      );

      console.log('Cyclic arb found!');
      console.log(`TX: ${transaction_hash}`);
      console.log(`Pair addresses: ${pairAddresses}`);
      console.log(`BASE: ${initialToken}`);
      console.log(`route: ${route}`);
      console.log(`Amount in: ${initialAmount.toString()}\nAmount out: ${lastAmount.toString()}`);
      console.log(`Profit: ${profit}`);
      console.log('\n');
    }
  }
}
