const ethers = require('ethers');
const { abi: IUniswapV2FactoryABI } = require('@uniswap/v2-periphery/build/IUniswapV2Factory.json');
const { abi: IUniswapV2RouterABI } = require('@uniswap/v2-periphery/build/IUniswapV2Router02.json');

const EXCHANGES = {
  PANCAKE: {
    ROUTER_ABI: IUniswapV2RouterABI,
    FACTORY_ABI: IUniswapV2FactoryABI,
    ROUTER_ADDRESS: '0x10ed43c718714eb63d5aa57b78b54704e256024e',
    FACTORY_ADDRESS: '0xca143ce32fe78f1f7019d7d551a6402fc5350c73',
  },
  MDEX: {
    ROUTER_ABI: IUniswapV2RouterABI,
    FACTORY_ABI: IUniswapV2FactoryABI,
    ROUTER_ADDRESS: '0x7DAe51BD3E3376B8c7c4900E9107f12Be3AF1bA8',
    FACTORY_ADDRESS: '0x3CD1C46068dAEa5Ebb0d3f55F6915B10648062B8',
  },
};

module.exports = {
  EXCHANGES,
  getRouterAndFactory: (exchange, provider) => ({
    ROUTER: new ethers.Contract(EXCHANGES[exchange].ROUTER_ADDRESS, EXCHANGES[exchange].ROUTER_ABI, provider),
    FACTORY: new ethers.Contract(EXCHANGES[exchange].FACTORY_ADDRESS, EXCHANGES[exchange].FACTORY_ABI, provider),
  }),
};
