const ethers = require('ethers');
const BigNumber = require('bignumber.js');
const { abi: IUniswapV2Pair } = require('@uniswap/v2-periphery/build/IUniswapV2Pair.json');
const { LOCAL_PROVIDER } = process.env;

const bscProvider = new ethers.providers.JsonRpcProvider(LOCAL_PROVIDER);

function setReservesListener(_arbitragePairs, _tokens, _callback) {
  Object.keys(_arbitragePairs).forEach((pairKey) => {
    const pair = new ethers.Contract(_arbitragePairs[pairKey], IUniswapV2Pair, bscProvider);
    const filter = pair.filters.Sync();
    const parts = pairKey.split('_');
    const token0Symbol = parts[0];
    const token1Symbol = parts[1];
    const token0Address = _tokens[token0Symbol];
    const token1Address = _tokens[token1Symbol];
    const [tokenA, tokenB] = token0Address > token1Address ? [token1Symbol, token0Symbol] : [token0Symbol, token1Symbol];

    // const [reserve0, reserve1] = await pair.getReserves();
    // const [reserveA, reserveB] = token0Address > token1Address ? [reserve1, reserve0] :[reserve0, reserve1];

    pair.on(filter, (reserve0, reserve1, event) => {
      const [reserveA, reserveB] = token0Address > token1Address ? [reserve1, reserve0] :[reserve0, reserve1];

      _callback(pairKey, event);

      // console.log({
      //   key: pairKey,
      //   event: 'reservesUpdated',
      //   [tokenA]: BigNumber(formatEther(reserveA)),
      //   [tokenB]: BigNumber(formatEther(reserveB)),
      // });
    });
  });
}

module.exports = { setReservesListener };
