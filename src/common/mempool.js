require('dotenv').config({ path: '../../../../.env' });
const _ = require('lodash');
const { setIntervalAsync } = require('set-interval-async/dynamic');
const ethers = require('ethers');
const { BSC_PROVIDER } = process.env;
const bscProvider = new ethers.providers.JsonRpcProvider(BSC_PROVIDER);

function sortByGasAndfilterPendingTxsByToAddress(pendingTxs, toFilter) {
  const pending = pendingTxs
    .map((pendingNonce) => pendingNonce.filter((nonceObj) => toFilter.length > 0 ? toFilter.includes(nonceObj.to) : true))
    .filter((pendingNonce) => !!pendingNonce.length);
  return _.orderBy(
    pending,
    (pendingNonce) => parseInt(pendingNonce[0].gasPrice),
    'desc',
  );
}

async function mempool(mempoolCallback, toFilter) {
  const blockNumber = await bscProvider.getBlockNumber();
  console.log(blockNumber);

  const txPoolContent = await bscProvider.send('txpool_content', []);
  // console.log(`${Object.keys(txPoolContent.pending).length} pending tx`);
  // console.time('Mapping transactions:');
  const pendingNonces = Object.values(txPoolContent.pending).map((pendingNonce) => Object.values(pendingNonce));
  const filteredData = sortByGasAndfilterPendingTxsByToAddress(pendingNonces, toFilter);

  mempoolCallback(filteredData);
  // console.timeEnd('Mapping transactions:');
}

function setupMempoolInterval(mempoolCallback, toFilter) {
  setIntervalAsync(mempool, 100, mempoolCallback, toFilter);
}

setupMempoolInterval(() => false, []);

module.exports = { setupMempoolInterval };
