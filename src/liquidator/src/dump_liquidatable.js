const args = process.argv.slice(2);
const configuration = args[0];

global.currentConfiguration = require(configuration);

const { sync } = require('./synchronization');
const { BigNumber } = require('bignumber.js');
const { writeFileSync } = require("fs")
const { getDBConnection } = require('./utils');
const { getLiquidatableAccounts } = require('./getLiquidatableAccounts');

async function run() {
  await sync(1000);

  const db = getDBConnection();

  const pvData = db.prepare(`
    SELECT internal_name, value
    FROM PROTOCOL_VALUES WHERE internal_name
    IN ('close_factor_mantissa', 'liquidation_incentive_mantissa');
  `).all();

  const closeFactor = pvData.filter(pv => pv.internal_name === "close_factor_mantissa")[0].value;
  const liqIncent = pvData.filter(pv => pv.internal_name === "liquidation_incentive_mantissa")[0].value;

  console.log("closeFactor", closeFactor);
  console.log("liqIncent", liqIncent);

  const liqAccs = getLiquidatableAccounts(closeFactor, liqIncent);

  let totalRevenue = new BigNumber(0);

  const data = [];

  for (acc of liqAccs) {
    totalRevenue = totalRevenue.plus(acc.revenue);
    data.push({
      account: acc.account,
      shortfall: acc.shortfall / (10**18),
      topRepayCToken: acc.topRepayCToken,
      repayAmount: acc.repayAmount.toString(10) / (10**18),
      revenue: acc.revenue.toString(10) / (10**18),
      topCollateral: acc.topCollateral.toString(10) / (10**18),
      topCollateralToken: acc.topCollateralToken,
      topCollateralCBalance: acc.topCollateralCBalance,
      repayUnderlyingPrice: acc.repayUnderlyingPrice,
    });
  }

  console.log(`Total revenue ${totalRevenue.toString(10) / (10**18)}`);

  writeFileSync(`./liquidatable_accounts_${new Date().toISOString()}.json`, JSON.stringify(data));
}

run();
