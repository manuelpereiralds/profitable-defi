const { getDBConnection } = require('./utils');

function createTables() {
  const db = getDBConnection();

  db.exec(`
    CREATE TABLE IF NOT EXISTS LIQUIDATIONS
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255),
      liquidator VARCHAR(255),
      borrower VARCHAR(255),
      repay_amount VARCHAR(255),
      ctoken_collateral VARCHAR(255),
      seize_tokens VARCHAR(255),
      block_number BIGINT,
      transaction_hash VARCHAR(255) UNIQUE NOT NULL,
      log_index VARCHAR(255)
    )
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS BORROWS
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      account VARCHAR(255),
      ctoken VARCHAR(255),
      account_borrows VARCHAR(255),
      account_ctoken_unique_id VARCHAR(255) UNIQUE NOT NULL,
      block_number BIGINT,
      transaction_hash VARCHAR(255),
      log_index VARCHAR(255)
    )
  `);

  db.exec(`
    CREATE UNIQUE INDEX IF NOT EXISTS idx_BORROWS_account_ctoken 
    ON BORROWS (account, ctoken);
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS CTOKENS_BORROW_OLDER_INDEXES
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255) NOT NULL,
      borrow_index VARCHAR(255),
      ctoken_block_log_unique_id VARCHAR(255) UNIQUE NOT NULL,
      block_number BIGINT,
      log_index VARCHAR(255)
    )
  `);

  db.exec(`
    CREATE UNIQUE INDEX IF NOT EXISTS idx_CTOKENS_BORROW_OLDER_INDEXES_ctoken_block_number 
    ON CTOKENS_BORROW_OLDER_INDEXES (ctoken, block_number);
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS CTOKENS_METADATA
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255) UNIQUE NOT NULL,
      borrow_index VARCHAR(255),
      exchange_rate VARCHAR(255),
      underlying_price VARCHAR(255),
      block_number BIGINT
    )
  `);

  db.exec(`
    CREATE UNIQUE INDEX IF NOT EXISTS idx_CTOKENS_METADATA_ctoken 
    ON CTOKENS_METADATA (ctoken);
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS ACCOUNT_BALANCES
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255),
      account VARCHAR(255),
      balance VARCHAR(255),
      status TINYINT(1) DEFAULT 0,
      account_ctoken_unique_id VARCHAR(255) UNIQUE NOT NULL,
      market_entered_block_number BIGINT DEFAULT -1,
      last_block_number BIGINT,
      initial_block_number BIGINT
    )
  `);

  db.exec(`
    CREATE UNIQUE INDEX IF NOT EXISTS idx_ACCOUNT_BALANCES_account_ctoken 
    ON ACCOUNT_BALANCES (account, ctoken);
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS COLLATERAL_CAP_TOKENS_ACCOUNTS
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255),
      account VARCHAR(255),
      balance VARCHAR(255),
      account_ctoken_unique_id VARCHAR(255) UNIQUE NOT NULL,
      block_number BIGINT,
      transaction_hash VARCHAR(255),
      log_index VARCHAR(255)
    )
  `);

  db.exec(`
    CREATE UNIQUE INDEX IF NOT EXISTS idx_COLLATERAL_CAP_TOKENS_ACCOUNTS_account_ctoken 
    ON COLLATERAL_CAP_TOKENS_ACCOUNTS (account, ctoken);
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS MARKET_EVENTS
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255),
      account VARCHAR(255),
      event VARCHAR(255),
      account_ctoken_unique_id VARCHAR(255) UNIQUE NOT NULL,
      block_number BIGINT,
      transaction_hash VARCHAR(255),
      log_index VARCHAR(255)
    )
  `);

  db.exec(`
    CREATE UNIQUE INDEX IF NOT EXISTS idx_MARKET_EVENTS_account_ctoken 
    ON MARKET_EVENTS (account, ctoken);
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS ACCOUNT_TRANSFERS
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255),
      account VARCHAR(255),
      amount VARCHAR(255),
      account_ctoken_hash_index_unique_id VARCHAR(255) UNIQUE NOT NULL,
      block_number BIGINT,
      transaction_hash VARCHAR(255),
      log_index VARCHAR(255)
    )
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS ACCOUNT_INTEREST_MINTABLE
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255) NOT NULL,
      account VARCHAR(255),
      amount VARCHAR(255),
      account_index_unique_id VARCHAR(255) UNIQUE NOT NULL,
      block_number BIGINT,
      transaction_hash VARCHAR(255),
      log_index VARCHAR(255)
    )
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS CTOKENS
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255) UNIQUE NOT NULL,
      symbol VARCHAR(10),
      decimals INTEGER,
      underlying_address VARCHAR(255),
      underlying_decimals INTEGER
    )
  `);

  db.exec(`
    CREATE UNIQUE INDEX IF NOT EXISTS idx_ctokens_ctoken 
    ON CTOKENS (ctoken);
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS CTOKENS_IMPLEMENTATION_UPGRADE_BLOCKS
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255) UNIQUE NOT NULL,
      block_number BIGINT
    )
  `);

  db.exec(`
    CREATE UNIQUE INDEX IF NOT EXISTS idx_CTOKENS_IMPLEMENTATION_UPGRADE_BLOCKS_ctoken 
    ON CTOKENS_IMPLEMENTATION_UPGRADE_BLOCKS (ctoken);
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS CTOKENS_COLLATERAL_FACTORS
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      ctoken VARCHAR(255) UNIQUE NOT NULL,
      collateral_factor VARCHAR(255),
      collateral_cap TINYINT(1) DEFAULT 0,
      block_number BIGINT
    )
  `);

  db.exec(`
    CREATE UNIQUE INDEX IF NOT EXISTS idx_CTOKENS_COLLATERAL_FACTORS_ctoken 
    ON CTOKENS_COLLATERAL_FACTORS (ctoken);
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS PROTOCOL_VALUES
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      internal_name VARCHAR(255) UNIQUE NOT NULL,
      value VARCHAR(255),
      block_number BIGINT
    )
  `);

  db.exec(`
    CREATE UNIQUE INDEX IF NOT EXISTS idx_PROTOCOL_VALUES_internal_name 
    ON PROTOCOL_VALUES (internal_name);
  `);

  db.exec(`
    CREATE TABLE IF NOT EXISTS SYNCHRONIZED_BLOCKS
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      created_at DEFAULT CURRENT_TIMESTAMP,
      block_number BIGINT
    )
  `);
}

module.exports = createTables;
