const { syncState } = require('./sync');
const { syncMarkets } = require('./sync/syncMarkets');
const { cleanDB } = require('./sync/cleanDB');
const { syncBorrowsDerivedData } = require('./sync/syncBorrowsDerivedData.js');
const { syncAccountBalances } = require('./sync/syncAccountBalances');
const createTables = require('./createTables');
const { waitForTransactions, getDBConnection } = require('./utils');
const { PROTOCOL, NETWORK } = currentConfiguration;
const { SYNC_INTERNALS: { [`${PROTOCOL}_${NETWORK}`]: syncInternalProtocol } } = require('./sync/PROTOCOLS');

async function sync(iterations = 1) {
  const db = getDBConnection();

  await createTables();

  const lastBlockSyncQueryResult = db.prepare(`
    SELECT * FROM SYNCHRONIZED_BLOCKS ORDER BY block_number DESC LIMIT 1  
  `).all();

  const lastSyncTimestamp = lastBlockSyncQueryResult.length > 0 ? new Date(lastBlockSyncQueryResult[0].created_at).getTime() : new Date().getTime();
  const daysPassed = (new Date().getTime() - lastSyncTimestamp)/1000/60/60/24;

  if (daysPassed > 5 || !lastBlockSyncQueryResult.length) {
    console.log(`${daysPassed} days have passed, doing internal synchronization`);
    await syncInternalProtocol();
  }

  console.time('Synchronization');
  await syncState(iterations);

  await waitForTransactions();

  syncAccountBalances();
  await waitForTransactions();

  cleanDB();

  await Promise.all([
    syncBorrowsDerivedData(),
    syncMarkets()
  ]);

  await waitForTransactions();

  console.timeEnd('Synchronization');
}

module.exports = { sync }
