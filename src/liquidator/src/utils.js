const path = require('path');
const { BigNumber } = require('bignumber.js');
const Database = require('better-sqlite3');
const { PROTOCOL, NETWORK } = currentConfiguration;

BigNumber.config({ DECIMAL_PLACES: 0, ROUNDING_MODE: BigNumber.ROUND_DOWN });

const GET_ORACLE = `
  SELECT value FROM PROTOCOL_VALUES
  WHERE internal_name = ?;
`;

const GET_ORACLE_PROXY = `
  SELECT value FROM PROTOCOL_VALUES
  WHERE internal_name = 'oracle_proxy';
`;

const GET_ORACLES = `
  SELECT internal_name, value FROM PROTOCOL_VALUES
  WHERE internal_name LIKE '%_oracle';
`;

const CLEAN_UNUSED_ENTRIES_INDEXES = `
DELETE FROM CTOKENS_BORROW_OLDER_INDEXES WHERE block_number NOT IN (
  SELECT b1.block_number FROM BORROWS b1
  INNER JOIN CTOKENS_BORROW_OLDER_INDEXES cboi
  ON (b1.ctoken = cboi.ctoken AND b1.block_number = cboi.block_number)
)`;

const GET_ACCOUNT_SUPPLIES = `
  SELECT acb.account, acb.ctoken as ctoken, acb.balance, ccf.collateral_factor, cm.exchange_rate, cm.underlying_price, cm.block_number, ct.underlying_decimals, ct.underlying_address, ct.symbol
  FROM ACCOUNT_BALANCES acb 
  INNER JOIN CTOKENS ct ON (ct.ctoken = acb.ctoken)
  INNER JOIN CTOKENS_METADATA cm ON (cm.ctoken = ct.ctoken)
  INNER JOIN CTOKENS_COLLATERAL_FACTORS ccf ON (ccf.ctoken = cm.ctoken)
  WHERE acb.balance > 0 AND acb.status = 1 AND ccf.collateral_factor > 0;
`;

const GET_ACCOUNT_BORROWS = `
  SELECT b2.account, b2.account_borrows, b2.ctoken as ctoken, ccf.collateral_factor, cm.borrow_index, cm.underlying_price, cboi.borrow_index as initial_borrow_index, ct.underlying_decimals, ct.underlying_address, ct.symbol, cm.block_number
  FROM BORROWS b2
  LEFT JOIN CTOKENS ct ON (ct.ctoken = b2.ctoken)
  LEFT JOIN CTOKENS_METADATA cm ON (cm.ctoken = ct.ctoken)
  LEFT JOIN CTOKENS_COLLATERAL_FACTORS ccf ON (ccf.ctoken = cm.ctoken)
  LEFT JOIN CTOKENS_BORROW_OLDER_INDEXES cboi ON (b2.ctoken = cboi.ctoken AND b2.block_number = cboi.block_number);
`;

const GET_TOP_BORROWED_TOKENS = `
  SELECT c.ctoken, c.symbol, t1.ctoken_borrows
  FROM (SELECT ctoken, COUNT(*) as ctoken_borrows FROM BORROWS GROUP BY ctoken ORDER BY COUNT(*) DESC) as t1
  INNER JOIN CTOKENS c ON c.ctoken = t1.ctoken WHERE t1.ctoken_borrows >= 15;
`;

const GET_MISSING_BORROWS_INDEXES = `
  SELECT b.ctoken, b.block_number
  FROM BORROWS b
  INNER JOIN CTOKENS ct ON (b.ctoken = ct.ctoken)
  LEFT JOIN CTOKENS_BORROW_OLDER_INDEXES cboi
  ON (b.ctoken = cboi.ctoken AND cboi.block_number = b.block_number)
  WHERE cboi.block_number IS NULL;
`;

const GET_COLLATERAL_CAP_CTOKENS = `SELECT ctoken FROM CTOKENS_COLLATERAL_FACTORS WHERE collateral_cap = 1;`;

const GET_CTOKENS_SQL_QUERY = `SELECT * FROM CTOKENS`;

let db;

function getDBConnection() {
  const dbPath = path.resolve(__dirname, `../../../database/${PROTOCOL}_${NETWORK}.sqlite`);

  if (!db) {
    db = new Database(dbPath);
    db.pragma('journal_mode = WAL');

    db.function('add_big_number', (a, b) => {
      return new BigNumber(a).plus(b).toString(10);
    });

    db.aggregate('add_all', {
      start: 0,
      step: (total, nextValue) => new BigNumber(total).plus(nextValue),
      result: total => total.toString(10)
    });
  }

  return db;
}

async function waitForTransactions() {
  let inTransaction = getDBConnection().inTransaction;

  return new Promise((resolve, reject) => {
    while (inTransaction) {
      inTransaction = getDBConnection().inTransaction;
    }
    resolve();
  });
}

module.exports = {
  getDBConnection,
  waitForTransactions,
  GET_CTOKENS_SQL_QUERY,
  GET_MISSING_BORROWS_INDEXES,
  CLEAN_UNUSED_ENTRIES_INDEXES,
  GET_ACCOUNT_BORROWS,
  GET_ACCOUNT_SUPPLIES,
  GET_TOP_BORROWED_TOKENS,
  GET_COLLATERAL_CAP_CTOKENS,
  GET_ORACLE,
  GET_ORACLES,
  GET_ORACLE_PROXY,
};
