const WebSocket = require("ws");
const BlocknativeSDK = require('bnc-sdk');
const bandOracleABI = require('../abis/stdReferenceAbi.json');
const sdkSetup = require('./sdk-setup');
const configuration = require('./band_oracle_configuration.json');
const { BigNumber } = require('bignumber.js');

const DAPP_ID = '7c73533a-81dc-4d4b-a1e5-18862566b72a';

const BANDTXpoolImpl = async (liquidationCallback) => {
  let blocknative;
  configuration[0].abi = bandOracleABI;

  const tokens = db.prepare(`
    SELECT symbol, underlying_address, underlying_decimals FROM CTOKENS;  
  `).all();

  const TOKENS_MAP = tokens.reduce((acc, next) => ({ ...acc, [next.symbol]: next}), {});

  const handleTransactionEvent = async (pendingTransaction) => {
    const {
      blockNumber,
      pendingBlockNumber,
      hash,
      gasPrice,
      gasPriceGwei,
      gas,
      contractCall: { params }
    } = pendingTransaction.transaction;
  
    console.time('Mempool handler');
    console.log(`TX ${hash} Pending at block ${pendingBlockNumber || blockNumber}`);
  
    const { _symbols, _rates } = params;
    const rates = _rates.split(',');
    const quoteIdx = _symbols.indexOf('BNB');
    let quoteRate;
  
    // Check if we need to call and retrieve quote from blockchain.
    if (quoteIdx < 0) {
      const oracle = new ethers.Contract("0xF5d5f2fcF50FF26b97Eb0BaAFBc14734aeDFbA27", bandOracleABI, provider);
      const [rate,] = await oracle.refs("BNB");
      quoteRate = rate.toString();
    } else {
      quoteRate = rates[quoteIdx];
    }
  
    const overrides = {};
  
    // Loop through symbols
    for (idx in _symbols) {
      const symbol = _symbols[idx];
      // If symbol exists within protocol ecosystem.
      if (!!TOKENS_MAP[symbol]) {
        const rate = rates[idx];
        const decimals = TOKENS_MAP[symbol].underlying_decimals;
  
        const price = new BigNumber(rate).times(1e18).div(quoteRate).times(10**(18-decimals)).toString(10);
  
        overrides[symbol] = price;
      }
    }

    await liquidationCallback(overrides);
  
    console.timeEnd('Mempool handler');
  };

  // initialize the SDK
  const blocknative = new BlocknativeSDK({
    dappId: DAPP_ID,
    networkId: 56,
    transactionHandlers: [handleTransactionEvent],
    ws: WebSocket
  });

  return sdkSetup(blocknative, configuration);
};

module.exports = { BANDTXpoolImpl }
