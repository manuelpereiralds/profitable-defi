const { BigNumber } = require('bignumber.js');
const { getAccountsWithAssets } = require('./functions');
const { INTEREST_TOKEN } = currentConfiguration;

BigNumber.config({ DECIMAL_PLACES: 0, ROUNDING_MODE: BigNumber.ROUND_DOWN });

function getLiquidatableAccounts(
  closeFactorMantissa = "500000000000000000",
  liquidationIncentiveMantissa = "1080000000000000000",
  overrides = {},
) {
  const accountsWithAssets = getAccountsWithAssets(overrides);
  const liquidatableAccounts = [];

  accountsWithAssets.forEach(({ assets, account, shortfall }) => {
    if (shortfall > 0) {
      let topRepay = new BigNumber(0);
      let topRepayCToken = '';
      let repayUnderlying;
      let topCollateral = new BigNumber(0);
      let topCollateralToken = '';
      let topCollateralCBalance = new BigNumber(0);
      let seizeUnderlyingPrice;
      let repayUnderlyingPrice;
      let seizeUnderlying;

      assets.forEach(({ borrowETH, cToken, underlying, underlyingPrice }) => {
        if (!!INTEREST_TOKEN && cToken.toLowerCase() === INTEREST_TOKEN.toLowerCase()) return;
        if (topRepay.lt(borrowETH)) {
          topRepay = borrowETH;
          topRepayCToken = cToken;
          repayUnderlying = underlying;
          repayUnderlyingPrice = underlyingPrice;
        }
      });
      assets.forEach(({ totalCollateralETH, cToken, cTokenBalance, underlying, underlyingPrice }) => {
        if (topCollateral.lt(totalCollateralETH)) {
          topCollateralCBalance = cTokenBalance;
          topCollateral = totalCollateralETH;
          topCollateralToken = cToken;
          seizeUnderlying = underlying;
          seizeUnderlyingPrice = underlyingPrice;
        }
      });

      // 2% slippage to prevent paying more than available close factor.
      let repayAmount = topRepay.times(closeFactorMantissa).times(98).div(100).div(1e18);
      let repayAmountPlusLiquidationIncentive = repayAmount.times(liquidationIncentiveMantissa).div(1e18);

      // Check if total 50% amount can be paid (If borrower has enough collateral)
      if (repayAmountPlusLiquidationIncentive.gt(topCollateral)) {
        const diff = repayAmountPlusLiquidationIncentive.minus(topCollateral);
        // Substract diff with Top collateral picked and substract as well 1% to stay in a safe range.
        repayAmount = repayAmount.minus(diff).times(99).div(100);
      }

      // Final amount to be seized from borrower collateral.
      repayAmountPlusLiquidationIncentive = repayAmount.times(liquidationIncentiveMantissa).div(1e18);
      const revenue = repayAmountPlusLiquidationIncentive.minus(repayAmount);

      const THRESHOLD = new BigNumber(0.03).times(1e18);

      if (revenue.lt(THRESHOLD)) return;

      liquidatableAccounts.push({
        account,
        shortfall,
        topRepayCToken,
        repayAmount,
        revenue,
        topCollateral,
        topCollateralToken,
        topCollateralCBalance,
        repayUnderlying,
        seizeUnderlying,
        seizeUnderlyingPrice,
        repayUnderlyingPrice,
      });
    }
  });

  // sort by revenue
  const sorted = liquidatableAccounts.sort((a, b) => b.revenue.minus(a.revenue));

  return sorted;
}

module.exports = { getLiquidatableAccounts }
