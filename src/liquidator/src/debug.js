const args = process.argv.slice(2);
const configuration = args[0];

global.currentConfiguration = require(configuration);

const ethers = require('ethers');
const cERC20Abi = require('./abis/cERC20.json');
const { hexlify, hexStripZeros } = ethers.utils;
const { waitForTransactions } = require('./utils');
const { URL, INTEREST_TOKEN } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

const TRANSFER_TOPIC_HASH = '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef';
const iface = new ethers.utils.Interface(cERC20Abi);

const lista = [];

const FEE_ADDRESS = "0xf322942f644a996a617bd29c16bd7d231d9f35e9";
const HASH_FEE_ADDRESS = "0x000000000000000000000000f322942f644a996a617bd29c16bd7d231d9f35e9";

// https://bscscan.com/tx/0x6aa8c8c47daaf2ca9848f60098ef03f3f6ccd7bc8c407ce265037cb652225a13#eventlog
// MintFee + MintVAI sample tx

const sync = async (
  latestBlockSynchronized,
  latestBlockMined
) => {
  const pastEvents = await provider.send('eth_getLogs', [{
    address: INTEREST_TOKEN,
    fromBlock: hexStripZeros(hexlify(latestBlockSynchronized)),
    toBlock: hexStripZeros(hexlify(latestBlockMined)),
    topics: [
      TRANSFER_TOPIC_HASH,
      [ethers.constants.HashZero, "0x00000000000000000000000000000B20f0F6a3a212aA6b85106709cd5941457c"],
      ["0x00000000000000000000000000000B20f0F6a3a212aA6b85106709cd5941457c", ethers.constants.HashZero, HASH_FEE_ADDRESS],
    ]
  }]);

  pastEvents.forEach((pastEvent) => {
    const { args } = iface.parseLog(pastEvent);
    const { transactionHash, logIndex } = pastEvent;
    const blockNumber = Number(pastEvent.blockNumber);
    const cToken = INTEREST_TOKEN.toLowerCase();
    const fromAccount = args[0];
    const toAccount = args[1];

    const amount = args[2].toString();

    if (fromAccount.toLowerCase() !== ethers.constants.AddressZero) {
      lista.push({
        cToken,
        fromAccount,
        amount: '-' + amount,
        id: fromAccount + transactionHash + logIndex,
        blockNumber,
        transactionHash,
        logIndex
      });
    }

    if (toAccount.toLowerCase() !== ethers.constants.AddressZero) {
      lista.push({
        cToken,
        toAccount,
        amount,
        id: toAccount + transactionHash + logIndex,
        blockNumber,
        transactionHash,
        logIndex
      });
    }
  });

  await waitForTransactions();
}

async function syncDebug() {
  // const latestBlockMined = await provider.getBlockNumber();
  const latestBlockMined = 7941873;
  const stepSize = 5000;

  let fromBlock = 2471512;
  // let fromBlock = 100320;
  console.log(`fromBlock: ${fromBlock} -> latestBlockMined: ${latestBlockMined}`);

  let i = 0;
  let toBlock = 0;

  do {
    toBlock = fromBlock + stepSize;
    if (toBlock > latestBlockMined) {
      toBlock = latestBlockMined;
    }

    console.log(`Debug:Synchronizing ${fromBlock} -> ${toBlock}`);

    await Promise.all([
      sync(fromBlock, toBlock),
    ]);

    fromBlock = toBlock;
    i++;
  } while (toBlock != latestBlockMined)

  console.log(lista);

  console.log(`Total iterations done: ${i}`);
  console.log(`synchronization complete: ${toBlock}`);
}

syncDebug();
