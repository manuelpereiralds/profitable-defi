require('dotenv').config({ path: '../../../.env' });

const args = process.argv.slice(2);
const mode = args[0];
const configuration = args[1];

// setup global
global.currentConfiguration = require(configuration);

const { abi: LiquidatorAbi } = require('../../../artifacts/contracts/LIQUIDATIONS/Liquidator.sol/Liquidator.json');
const ComptrollerABI = require("./abis/ComptrollerImpl.json");
const { BigNumber } = require("bignumber.js");
const ethers = require('ethers');
const { getLiquidatableAccounts } = require('./getLiquidatableAccounts');
const { sync } = require('./synchronization');
const { getDBConnection } = require('./utils');
const { getOneInchSwap, getAmountAfterPriceImpact } = require('./functions');
const { MNEMONIC } = process.env;

BigNumber.config({ DECIMAL_PLACES: 0, ROUNDING_MODE: BigNumber.ROUND_DOWN });

const db = getDBConnection();
const WBNB = "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c";


// node liquidation.js txpool ./config_cream.json > log.txt
// node liquidation.js basic ./config_venus.json > log.txt

async function run() {
  let IS_RUNNING = false;

  const { WS_URL, LIQUIDATOR_ADDRESS, MEMPOOL_FUNC_NAME, COMPTROLLER } = currentConfiguration;

  const provider = new ethers.providers.WebSocketProvider(WS_URL);
  const wallet = ethers.Wallet.fromMnemonic(MNEMONIC);
  const mainAccount = wallet.connect(provider);

  // const comptroller = new ethers.Contract(COMPTROLLER, ComptrollerABI, mainAccount);
  
  const liquidatorContract = new ethers.Contract(LIQUIDATOR_ADDRESS, LiquidatorAbi, mainAccount);

  const pvData = db.prepare(`
    SELECT internal_name, value
    FROM PROTOCOL_VALUES WHERE internal_name
    IN ('close_factor_mantissa', 'liquidation_incentive_mantissa');
  `).all();

  const closeFactor = pvData.filter(pv => pv.internal_name === "close_factor_mantissa")[0].value;
  const liqIncent = pvData.filter(pv => pv.internal_name === "liquidation_incentive_mantissa")[0].value;

  const blackList = [
    "0x032daf5bd6DF0CddA648d11071f4157e61703722".toLowerCase()
  ];

  const doLiquidate = async (liquidation) => {
    const {
      repayAmount,
      repayUnderlyingPrice,
      seizeUnderlyingPrice,
      topRepayCToken,
      topCollateralToken,
      repayUnderlying,
      seizeUnderlying,
      revenue,
      account,
      shortfall,
    } = liquidation;

    if (blackList.indexOf(account.toLowerCase()) > -1) {
      console.log(`${account} Borrower invalid.`);
      return;
    }

    console.time("Liquidating account");

    const repayTokenAmount = new BigNumber(repayAmount)
      .dividedToIntegerBy(repayUnderlyingPrice)
      .times(1e18)
      .toString(10);

    const seizedTokenAmount = new BigNumber(repayAmount)
      .times(liqIncent)
      .dividedToIntegerBy(seizeUnderlyingPrice)
      .toString(10);

    console.log("Account", account);
    console.log("repayAmountBNB", repayAmount.toString(10));
    console.log("shortfall", shortfall/1e18);
    console.log("topRepayCToken", topRepayCToken);
    console.log("topCollateralToken", topCollateralToken);
    console.log("repayUnderlying", repayUnderlying);
    console.log("seizeUnderlying", seizeUnderlying);
    console.log("repayTokenAmount", repayTokenAmount);
    console.log("seizedTokenAmount", seizedTokenAmount);
    console.log("revenue", revenue.toString(10) / 1e18);

    let finalAmount = repayAmount.toString(10);
    let fromTokenAmount;
    let toTokenAmount;
    let swapData0 = ethers.constants.HashZero;
    let swapData1 = ethers.constants.HashZero;

    if (repayUnderlying.toLowerCase() !== WBNB.toLowerCase()) {
      const [txData0, fromTokenAmount0, toTokenAmount1, protocols0] = await getOneInchSwap(
        liquidatorContract.address,
        WBNB,
        repayUnderlying,
        repayAmount.toString(10)
      );

      swapData0 = txData0;
      fromTokenAmount = fromTokenAmount0;
    }

    if (seizeUnderlying.toLowerCase() !== WBNB.toLowerCase()) {
      const [txData1, fromTokenAmount2, toTokenAmount3, protocols1] = await getOneInchSwap(
        liquidatorContract.address,
        seizeUnderlying,
        WBNB,
        seizedTokenAmount
      );

      swapData1 = txData1;
      toTokenAmount = toTokenAmount3;
    }

    if (!!fromTokenAmount && !!toTokenAmount) {
      finalAmount = getAmountAfterPriceImpact(repayAmount.toString(10), fromTokenAmount, toTokenAmount);
    }

    console.log("finalAmount:", finalAmount);

    const tx = await liquidatorContract.populateTransaction.liq_2627(
      account, // borrower
      topRepayCToken, // repayCToken
      topCollateralToken, // collateral to seize
      finalAmount,
      swapData0,
      swapData1
    );
    tx.gasLimit = 5000000; // 5M
    tx.gasPrice = ethers.utils.parseUnits('10.5', 'gwei');

    console.timeEnd("Liquidating account");

    const txSent = await mainAccount.sendTransaction(tx);
    console.log("Waiting for transaction");
    const txReceipt = await txSent.wait();
    console.log(txReceipt);
  };

  // Every block
  provider.on('block', async (blockNumber) => {
    if (IS_RUNNING === true) return;

    IS_RUNNING = true;

    console.log(`Listening received block ${blockNumber}`);

    // Sync DB
    await sync(100);

    console.time("Searching liquidatable accounts");

    const liqAccs = getLiquidatableAccounts(closeFactor, liqIncent);

    if (liqAccs.length > 0) {
      console.log(`${liqAccs.length} accounts found to be liquidatable.`)
    } else {
      console.log("No liquidations have been found");
    }

    console.timeEnd("Searching liquidatable accounts");

    for (liquidation of liqAccs) {
      try {
        await doLiquidate(liquidation);
      } catch (err) {
        console.log(err.message);
        console.log("Error while trying to send transaction")
      }
    }

    IS_RUNNING = false;
  });

  if (mode === "txpool") {
    const { [MEMPOOL_FUNC_NAME]: TXpoolImpl } = require('./configurations/index');

    TXpoolImpl(async (overrides) => {
      // Check and Wait for SYNC to finish.
      while (IS_RUNNING) {
        await new Promise(r => setTimeout(r, 50));  // 50ms
      }

      const liqAccs = getLiquidatableAccounts(closeFactor, liqIncent, overrides);
  
      if (liqAccs.length > 0) {
        console.log(`${liqAccs.length} accounts found to be liquidatable.`)
      } else {
        console.log("No liquidations have been found");
      }
  
      for (liquidation of liqAccs) {
        await doLiquidate(liquidation);
      }
    });
  }
}

run().then(() => {
  console.log("Running.");  
}).catch((err) => {
  console.log(err);
});
