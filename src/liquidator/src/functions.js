const fetch = require('node-fetch');
const { BigNumber } = require('bignumber.js');
const { getDBConnection, GET_ACCOUNT_BORROWS, GET_ACCOUNT_SUPPLIES } = require('./utils');
const { INTEREST_TOKEN } = currentConfiguration;

BigNumber.config({ DECIMAL_PLACES: 0, ROUNDING_MODE: BigNumber.ROUND_DOWN });

const protocols = [
  "BSC_BI_SWAP",
  "BSC_KYBER_DMM",
  "BSC_PMMX",
  "BSC_UNIFI",
  "JETSWAP",
  "GAMBIT_FINANCE",
  // "ACSI_FINANCE",
  "BSC_ONE_INCH_LIMIT_ORDER",
  "BURGERSWAP",
  "WARDEN",
  "WAULTSWAP",
  "MDEX",
  "PANCAKESWAP_V2",
  "PANCAKESWAP",
  "WBNB",
  "VENUS",
  "BAKERYSWAP",
  "BSC_ONE_INCH_LP",
  "JULSWAP",
  // "ACRYPTOS",
  // "BSC_DODO",
  "APESWAP",
  "SPARTAN",
  "BELTSWAP",
  "VPEGSWAP",
  "VSWAP",
  "HYPERSWAP",
  "BSC_DODO_V2",
  "ELLIPSIS_FINANCE",
  // "BSC_SMOOTHY_FINANCE",
  // "NERVE",
  // "CHEESESWAP",
  "BSC_PMM1",
  "URANIUM"
];

function calcBorrowBalance(borrowedAmount, interestIndex, initialInterestIndex) {
  return new BigNumber(borrowedAmount).times(interestIndex).dividedToIntegerBy(initialInterestIndex || 1);  
}

function getAccountLiquidity(assets = []) {
  let accountSumCollateral = new BigNumber(0);
  let sumBorrowPlusEffects = new BigNumber(0);

  assets.forEach(({ borrowETH, supplyETH })=> {
    accountSumCollateral = accountSumCollateral.plus(supplyETH);
    sumBorrowPlusEffects = sumBorrowPlusEffects.plus(borrowETH);
  });

  let expectedLiquidity, expectedShortFall;

  if (accountSumCollateral.gt(sumBorrowPlusEffects)) {
    expectedLiquidity = accountSumCollateral.minus(sumBorrowPlusEffects).toString(10);
    expectedShortFall = '0';
  } else {
    expectedLiquidity = '0';
    expectedShortFall = sumBorrowPlusEffects.minus(accountSumCollateral).toString(10);
  }

  return [expectedLiquidity, expectedShortFall];
}

function calculateAssetsLiquidity(assetsMap) {
  const assets = Object.keys(assetsMap);
  const results = [];

  for (cToken of assets) {
    const asset = assetsMap[cToken];

    const {
      balance: cTokenBalance = 0,
      account_borrows = 0,
      borrow_index = 0,
      initial_borrow_index = 0,
      exchange_rate: exchangeRate = 0,
      collateral_factor: collateralFactor,
      underlying_price: oraclePrice,
      underlying_address: underlying,
      block_number
    } = asset;
    const blockNumber = block_number;

    if (INTEREST_TOKEN && cToken.toLowerCase() === INTEREST_TOKEN.toLowerCase()) {
      results.push({
        supplyETH: 0,
        borrowETH: account_borrows,
        totalCollateralETH: 0,
        underlyingPrice: (1e18).toString(),
        underlying,
        cToken,
        blockNumber,
        cTokenBalance: 0,
      });

      continue;
    }

    const borrowBalanceStored = calcBorrowBalance(account_borrows, borrow_index, initial_borrow_index);

    const tokensToDenom = new BigNumber(collateralFactor)
    .times(exchangeRate)
    .div(1e18)
    .times(oraclePrice)
    .div(1e18);

    const supplyBalance = tokensToDenom.times(cTokenBalance).dividedToIntegerBy(1e18);
    const borrowBalance = new BigNumber(oraclePrice).times(borrowBalanceStored).dividedToIntegerBy(1e18);

    const totalCollateral = new BigNumber(cTokenBalance)
    .times(exchangeRate)
    .div(1e18)
    .times(oraclePrice)
    .div(1e18);

    results.push({
      supplyETH: supplyBalance,
      borrowETH: borrowBalance,
      totalCollateralETH: totalCollateral,
      underlyingPrice: oraclePrice,
      underlying,
      cToken,
      blockNumber,
      cTokenBalance,
      borrowBalanceStored: borrowBalanceStored.toString(10),
    });
  }
  return results;
}

function getAccountsWithAssets(overrides = {}) {
  const db = getDBConnection();

  const accountSupplies = db.prepare(GET_ACCOUNT_SUPPLIES).all();
  const accountBorrows = db.prepare(GET_ACCOUNT_BORROWS).all();

  const accountAssets = {};

  accountBorrows.forEach((accountBorrow) => {
    const {
      ctoken,
      account,
      account_borrows,
      borrow_index,
      initial_borrow_index,
      collateral_factor,
      underlying_price,
      block_number,
      underlying_address,
      symbol,
    } = accountBorrow;
    const underlyingSymbol = !!symbol ? symbol.slice(2) : '';

    accountAssets[account] = {
      ...(accountAssets[account] || {}),
      [ctoken]: {
        ...(accountAssets[account] && accountAssets[account][ctoken] || {}),
        account_borrows,
        borrow_index,
        initial_borrow_index,
        collateral_factor,
        block_number,
        underlying_address,
        underlying_price: overrides[underlyingSymbol] !== undefined ? overrides[underlyingSymbol] : underlying_price || (1e18).toString(),
      }
    }
  });

  accountSupplies.forEach((accountSupply) => {
    const {
      ctoken,
      account,
      balance,
      exchange_rate,
      collateral_factor,
      underlying_price,
      block_number,
      underlying_address,
      symbol,
    } = accountSupply;
    const underlyingSymbol = symbol.slice(2);

    accountAssets[account] = {
      ...(accountAssets[account] || {}),
      [ctoken]: {
        ...(accountAssets[account] && accountAssets[account][ctoken] || {}),
        balance,
        exchange_rate,
        collateral_factor,
        block_number,
        underlying_address,
        underlying_price: overrides[underlyingSymbol] !== undefined ? overrides[underlyingSymbol] : underlying_price,
      }
    }
  });

  const accounts = [];

  Object.keys(accountAssets).map((account) => {
    const assets = calculateAssetsLiquidity(accountAssets[account]);

    if (assets.length < 1) {
      return;
    }

    const [expectedLiquidity, expectedShortFall] = getAccountLiquidity(assets);

    accounts.push({ account, assets, liquidity: expectedLiquidity, shortfall: expectedShortFall });
  });

  return accounts;
}

const getOneInchSwap = async (mainAddress, tokenIn, tokenOut, amountIn, availableProtocols = protocols) => {
  const chainId = 56;
  const slippage = 5;
  const url = (
    `https://api.1inch.exchange/v3.0/${chainId}/swap`+
    `?fromTokenAddress=${tokenIn}&toTokenAddress=${tokenOut}`+
    `&amount=${amountIn}&fromAddress=${mainAddress}&slippage=${slippage}`+
    `&destReceiver=${mainAddress}&complexityLevel=0&mainRouteParts=1&disableEstimate=true&protocols=${availableProtocols.join(',')}`
  )

  const response = await fetch(url);
  const parsedResponse = await response.json();

  return [parsedResponse.tx.data, parsedResponse.fromTokenAmount, parsedResponse.toTokenAmount, parsedResponse.protocols];
}

const getAmountAfterPriceImpact = (repayAmount, fromTokenAmount, toTokenAmount) => {
  let finalAmount = repayAmount;

  const diff = new BigNumber(toTokenAmount).minus(fromTokenAmount).div(1e18).toString();
  const slippageTable = [60, 55, 50, 45, 40, 35, 30, 25, 20];

  let idx = parseInt(diff);

  if (idx <= 2) {
    idx = Math.abs(idx >= slippageTable.length ? slippageTable.length-1 : idx);
    finalAmount = new BigNumber(repayAmount).times(slippageTable[idx]).dividedToIntegerBy(100).toString(10);
  }
  return finalAmount;
}

module.exports = { getAccountsWithAssets, calculateAssetsLiquidity, getAccountLiquidity, getOneInchSwap, getAmountAfterPriceImpact };
