const args = process.argv.slice(2);
const configuration = args[0];

global.currentConfiguration = require(configuration);

const ethers = require('ethers');
const ComptrollerAbi = require('./abis/ComptrollerImpl.json');
const { getAccountsWithAssets } = require('./functions');
const { sync } = require('./synchronization');
const { ARCHIVE, URL, COMPTROLLER } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

async function verify() {
  await sync(100);

  console.log('Starting verification.');

  const comptroller = new ethers.Contract(COMPTROLLER, ComptrollerAbi, provider);
  const accountsAssets = getAccountsWithAssets();

  console.log(`Working with ${accountsAssets.length}`);

  let invalidMatchesCount = 0;

  await Promise.all(accountsAssets.map(async (accountAssets, i) => {
  // for (let i = 0; i < accountsAssets.length; i++) {
    // const accountAssets = accountsAssets[i];
    const {
      account,
      liquidity,
      shortfall,
      assets,
    } = accountAssets;

    if (assets.length === 0) return;

    const latestBlockMined = assets[0].blockNumber || (assets[1] && assets[1].blockNumber);

    try {
      const [error, liquidityC, shortfallC] = await comptroller.getAccountLiquidity(account, { blockTag: latestBlockMined});
      console.log(`Index: ${i} - Using block ${latestBlockMined} for verification`);
  
      if (liquidityC.toString() !== liquidity) {
        invalidMatchesCount += 1;
        console.log(`Account ${account}: onchain liquidity: ${liquidityC} offchain liquidity ${liquidity}`);            
      }
  
      if (shortfallC.toString() !== shortfall) {
        invalidMatchesCount += 1;
        console.log(`Account ${account}: onchain shortfall: ${shortfallC} offchain shortfall ${shortfall}`);
      }
    } catch (err) {
      console.log(err.message);
    }
  }));

  console.log(`${invalidMatchesCount} of ${accountsAssets.length} total are wrong.`)

  console.log("Verification finished");
}

verify();
