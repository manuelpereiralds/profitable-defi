const args = process.argv.slice(2);
const configuration = args[0];

global.currentConfiguration = require(configuration);

const { PROTOCOL } = currentConfiguration;

const createTables = require('./createTables');
createTables();

const { createVenus, BSC_createCreamFI } = require('./setup');
const { sync } = require('./synchronization');

async function run() {
  if (PROTOCOL === "VENUS") {
    await createVenus();
  } else if (PROTOCOL === "CREAMFI") {
    await BSC_createCreamFI();
  }

  await sync(2400);
}

run();
