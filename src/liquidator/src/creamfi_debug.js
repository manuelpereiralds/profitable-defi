const ethers = require('ethers');
const abi = require('./abis/cERC20.json');
const { getDBConnection } = require('./utils');
const { URL } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

const db = getDBConnection();

async function run(){ 
  const stmt = `
  SELECT ab.account, ab.ctoken,ab.balance, ab.id, 0 as new_balance FROM ACCOUNT_BALANCES ab
  LEFT JOIN COLLATERAL_CAP_TOKENS_ACCOUNTS ccta
  ON (ab.ctoken = ccta.ctoken AND ccta.account = ab.account)
  INNER JOIN CTOKENS_COLLATERAL_FACTORS ccf ON (ab.ctoken = ccf.ctoken)
  --WHERE ccta.block_number >= ab.last_block_number
  WHERE ccta.account IS NULL AND ccf.collateral_cap = 1 AND ab.balance <>0 AND ab.status <>0 AND
  ab.market_entered_block_number > ab.last_block_number;
  `;

  const accs = db.prepare(stmt).all();

  let i = 0;

  let idsString = "";

  for (acc of accs.slice(70, 90)) {
    const token = new ethers.Contract(acc.ctoken, abi, provider);

    const [error, balance, borrowBalance] = await token.getAccountSnapshot(acc.account);

    if (balance.toString() !== acc.balance) {
      console.log("acc.ctoken", acc.ctoken);
      console.log("acc.account", acc.account)
      console.log("chain balance:", balance.toString());
      console.log("offchain balance:", acc.balance);

      idsString = `${idsString},${acc.id}`;
    }
    i = i + 1;
  }

  console.log(idsString);
}

run();