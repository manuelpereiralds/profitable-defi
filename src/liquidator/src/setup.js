const ethers = require('ethers');
const fetch = require('node-fetch');
const cERC20Abi = require('./abis/cERC20.json');
const ERC20Abi = require('./abis/ERC20.json');
const { getDBConnection } = require('./utils');
const createTables = require('./createTables');
const { URL, NATIVE_TOKEN, WRAPPED_ADDRESS } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

async function ETH_createCreamFI() {
  await createTables();

  const BASE_URL = 'https://api.cream.finance/api/v1/crtoken?comptroller=eth';

  const response = await fetch(BASE_URL);
  const crTokens = await response.json();
  await createCreamFI(crTokens);
}

async function BSC_createCreamFI() {
  await createTables();

  const BASE_URL = 'https://api.cream.finance/api/v1/crtoken?comptroller=bsc';

  const response = await fetch(BASE_URL);
  const crTokens = await response.json();
  await createCreamFI(crTokens);
}

async function createCreamFI(crTokens) {
  const db = getDBConnection();
  // Setup available tokens
  await Promise.all(crTokens.map(async (crToken) => {
    const crContract = new ethers.Contract(crToken.token_address, cERC20Abi, provider);
    const crDecimals = await crContract.decimals();

    let underlyingAddress = WRAPPED_ADDRESS, underlyingDecimals = 18;

    if (crToken.symbol !== `cr${NATIVE_TOKEN}`) {
      underlyingAddress = await crContract.underlying();
      const underlyingContract = new ethers.Contract(underlyingAddress, ERC20Abi, provider);
      underlyingDecimals = await underlyingContract.decimals();
    }

    const stmt = db.prepare(`
      INSERT INTO CTOKENS
        (ctoken, symbol, decimals, underlying_address, underlying_decimals)
      VALUES
        (?,?,?,?,?)
      ON CONFLICT(ctoken)
      DO NOTHING
    `);
    
    stmt.run(crToken.token_address.toLowerCase(), crToken.symbol, crDecimals, underlyingAddress, underlyingDecimals);
  }));
}

async function createVenus() {
  await createTables();

  const db = getDBConnection();

  const BASE_URL = "https://api.venus.io/api/governance/venus";
  const response = await fetch(BASE_URL);
  const { data: { markets: vTokens }} = await response.json();

  await Promise.all(vTokens.map(async (vToken) => {
    const stmt = db.prepare(`
      INSERT INTO CTOKENS
        (ctoken, symbol, decimals, underlying_address, underlying_decimals)
      VALUES
        (?,?,?,?,?)
      ON CONFLICT(ctoken)
      DO NOTHING
    `);

    stmt.run(
      vToken.address.toLowerCase(),
      vToken.symbol,
      8,
      vToken.underlyingAddress,
      vToken.underlyingDecimal
    );
  }));
}


module.exports = { ETH_createCreamFI, BSC_createCreamFI, createVenus };
