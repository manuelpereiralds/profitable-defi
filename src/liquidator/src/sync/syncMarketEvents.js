const ethers = require('ethers');
const { hexlify, hexStripZeros } = ethers.utils;
const comptrollerAbi = require('../abis/Comptroller.json');
const { getDBConnection, waitForTransactions } = require('../utils');
const { URL, COMPTROLLER } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

const MARKET_ENTERED_TOPIC = '0x3ab23ab0d51cccc0c3085aec51f99228625aa1a922b3a8ca89a26b0f2027a1a5';
const MARKET_EXITED_TOPIC = '0xe699a64c18b07ac5b7301aa273f36a2287239eb9501d81950672794afba29a0d';

const iface = new ethers.utils.Interface(comptrollerAbi);

const synchronizeMarketEvents = async (
  latestBlockSynchronized,
  latestBlockMined
) => {
  const db = getDBConnection();

  const pastEvents = await provider.send('eth_getLogs', [{
    address: COMPTROLLER,
    fromBlock: hexStripZeros(hexlify(latestBlockSynchronized)),
    toBlock: hexStripZeros(hexlify(latestBlockMined)),
    topics: [[MARKET_EXITED_TOPIC, MARKET_ENTERED_TOPIC]]
  }]);

  const stmt = db.prepare(`
    INSERT INTO MARKET_EVENTS
      (ctoken, account, event, account_ctoken_unique_id, block_number, transaction_hash, log_index)
    VALUES
      (?,?,?,?,?,?,?)
    ON CONFLICT(account_ctoken_unique_id)
    DO UPDATE SET event = excluded.event, block_number = excluded.block_number, transaction_hash = excluded.transaction_hash, log_index = excluded.log_index
  `);

  db.transaction(() => {
    pastEvents.forEach((pastEvent) => {
      const { args, name: eventName } = iface.parseLog(pastEvent);
      const blockNumber = Number(pastEvent.blockNumber);

      const cToken = args[0].toLowerCase();
      const account = args[1];

      stmt.run(
        cToken,
        account,
        eventName,
        account + cToken,
        blockNumber,
        pastEvent.transactionHash,
        pastEvent.logIndex
      );
    });
  })();

  await waitForTransactions();
}

module.exports = { synchronizeMarketEvents };
