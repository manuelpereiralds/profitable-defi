const ethers = require('ethers');
const ComptrollerAbi = require('../../../abis/Comptroller.json');
const oracleAbi = require('../../../abis/oracle.json');
const chainLinkRegistry = require('../../../abis/chainLinkFeedRegistry.json');
const { getDBConnection, GET_CTOKENS_SQL_QUERY } = require('../../../utils');
const { URL, COMPTROLLER } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

async function syncInternalValues() {
  const db = getDBConnection();

  const latestBlockMined = await provider.getBlockNumber();

  const comptroller = new ethers.Contract(COMPTROLLER, ComptrollerAbi, provider);

  const closeFactorMantissa = await comptroller.closeFactorMantissa();
  const liquidationIncentiveMantissa = await comptroller.liquidationIncentiveMantissa();
  const oracleProxyAddress = await comptroller.oracle();

  const oracleProxyContract = new ethers.Contract(oracleProxyAddress, oracleAbi, provider);

  const v1PriceOracleAddress = await oracleProxyContract.v1PriceOracle();
  const feedRegistryAddress = await oracleProxyContract.registry();
  const feedRegistry = new ethers.Contract(feedRegistryAddress, chainLinkRegistry, provider);

  const cTokens = db.prepare(GET_CTOKENS_SQL_QUERY).all();

  const stmt = db.prepare(`
    INSERT INTO PROTOCOL_VALUES
      (internal_name, value, block_number)
    VALUES
      (?,?,?)
    ON CONFLICT(internal_name)
    DO UPDATE SET value=?, block_number=?
  `);

  await Promise.all(cTokens.map(async (cToken) => {
    if (!cToken.underlying_address) return;

    const [baseAddress, quoteAddress] = await oracleProxyContract.aggregators(cToken.underlying_address);

    if (baseAddress === '0x0000000000000000000000000000000000000000' || quoteAddress === '0x0000000000000000000000000000000000000000') return;

    const aggregatorAddress = await feedRegistry.getFeed(baseAddress, quoteAddress);

    stmt.run(`${cToken.symbol}_oracle`, aggregatorAddress, latestBlockMined, aggregatorAddress, latestBlockMined);
  }));

  stmt.run('close_factor_mantissa', closeFactorMantissa.toString(), latestBlockMined, closeFactorMantissa.toString(), latestBlockMined);
  stmt.run('liquidation_incentive_mantissa', liquidationIncentiveMantissa.toString(), latestBlockMined, liquidationIncentiveMantissa.toString(), latestBlockMined);
  stmt.run('oracle_proxy', oracleProxyAddress, latestBlockMined, oracleProxyAddress, latestBlockMined);
  stmt.run('PROTOCOL_oracle', v1PriceOracleAddress, latestBlockMined, v1PriceOracleAddress, latestBlockMined);
}

module.exports = { syncInternalValues };
