const { syncInternalValues: syncInternalValuesETH } = require('./CREAMFI_ETH/syncInternalValues');
const { syncInternalValues: syncInternalValuesCreamBSC } = require('./CREAMFI_BSC/syncInternalValues');
const { syncInternalValues: syncInternalValuesVenus } = require('./VENUS/syncInternalValues');
const { synchronizeImplUpgradeBlocks } = require('../syncImplementationUpgradeBlocks');
const { syncTokenCollateralFactors } = require('../syncTokenCollateralFactors');

const SYNC_INTERNALS = {
  CREAMFI_ETH: async () => {
    await Promise.all([syncTokenCollateralFactors(),syncInternalValuesETH(),synchronizeImplUpgradeBlocks()]);
  },
  CREAMFI_BSC: async () => {
    await Promise.all([syncTokenCollateralFactors(),syncInternalValuesCreamBSC()]);
  },
  VENUS_BSC: async () => {
    await Promise.all([syncTokenCollateralFactors(),syncInternalValuesVenus()]);
  },
}

module.exports = { SYNC_INTERNALS };
