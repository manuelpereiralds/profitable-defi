const ethers = require('ethers');
const ComptrollerAbi = require('../../../abis/Comptroller.json');
const oracleAbi = require('../../../abis/CREAMFI_ORACLE_BSC.json');
const { getDBConnection } = require('../../../utils');
const { URL, COMPTROLLER } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

async function syncInternalValues() {
  const db = getDBConnection();

  const latestBlockMined = await provider.getBlockNumber();

  const comptroller = new ethers.Contract(COMPTROLLER, ComptrollerAbi, provider);

  const closeFactorMantissa = await comptroller.closeFactorMantissa();
  const liquidationIncentiveMantissa = await comptroller.liquidationIncentiveMantissa();
  const oracleProxyAddress = await comptroller.oracle();

  const oracleProxyContract = new ethers.Contract(oracleProxyAddress, oracleAbi, provider);

  const v1PriceOracleAddress = await oracleProxyContract.v1PriceOracle();
  const externalOracleAddress = await oracleProxyContract.ref();
  const refProxyContract = new ethers.Contract(externalOracleAddress, ["function ref() view returns (address)"], provider);

  const refOracleAddress = await refProxyContract.ref();

  const stmt = db.prepare(`
    INSERT INTO PROTOCOL_VALUES
      (internal_name, value, block_number)
    VALUES
      (?,?,?)
    ON CONFLICT(internal_name)
    DO UPDATE SET value=?, block_number=?
  `);

  stmt.run('creambsc_oracle', refOracleAddress, latestBlockMined, refOracleAddress, latestBlockMined);
  stmt.run('close_factor_mantissa', closeFactorMantissa.toString(), latestBlockMined, closeFactorMantissa.toString(), latestBlockMined);
  stmt.run('liquidation_incentive_mantissa', liquidationIncentiveMantissa.toString(), latestBlockMined, liquidationIncentiveMantissa.toString(), latestBlockMined);
  stmt.run('oracle_proxy', oracleProxyAddress, latestBlockMined, oracleProxyAddress, latestBlockMined);
  stmt.run('PROTOCOL_oracle', v1PriceOracleAddress, latestBlockMined, v1PriceOracleAddress, latestBlockMined);
}

module.exports = { syncInternalValues };
