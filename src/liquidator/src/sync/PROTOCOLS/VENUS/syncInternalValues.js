const ethers = require('ethers');
const ComptrollerAbi = require('../../../abis/Comptroller.json');
const oracleAbi = require('../../../abis/VENUS_CHAINLINK_ORACLE.json');
const oracleProxyAbi = require('../../../abis/AggregatorProxy.json')
const { getDBConnection, GET_CTOKENS_SQL_QUERY } = require('../../../utils');
const { URL, COMPTROLLER } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

async function syncInternalValues() {
  const db = getDBConnection();

  const latestBlockMined = await provider.getBlockNumber();

  const comptroller = new ethers.Contract(COMPTROLLER, ComptrollerAbi, provider);

  const closeFactorMantissa = await comptroller.closeFactorMantissa();
  const liquidationIncentiveMantissa = await comptroller.liquidationIncentiveMantissa();
  const oracleAddress = await comptroller.oracle();

  const oracleContract = new ethers.Contract(oracleAddress, oracleAbi, provider);

  const cTokens = db.prepare(GET_CTOKENS_SQL_QUERY).all();

  const stmt = db.prepare(`
    INSERT INTO PROTOCOL_VALUES
      (internal_name, value, block_number)
    VALUES
      (?,?,?)
    ON CONFLICT(internal_name)
    DO UPDATE SET value=?, block_number=?
  `);

  await Promise.all(cTokens.map(async (cToken) => {
    let externalOracleAddress;

    if (cToken.symbol === "vBNB") {
      externalOracleAddress = await oracleContract.getFeed(cToken.symbol);
    } else {
      externalOracleAddress = await oracleContract.getFeed(cToken.symbol.slice(1));
    }

    if (ethers.constants.AddressZero === externalOracleAddress) return;

    const externalOracleProxy = new ethers.Contract(externalOracleAddress, oracleProxyAbi, provider);

    const aggregatorAddress = await externalOracleProxy.aggregator();

    stmt.run(`${cToken.symbol}_oracle`, aggregatorAddress, latestBlockMined, aggregatorAddress, latestBlockMined);
  }));

  stmt.run('close_factor_mantissa', closeFactorMantissa.toString(), latestBlockMined, closeFactorMantissa.toString(), latestBlockMined);
  stmt.run('liquidation_incentive_mantissa', liquidationIncentiveMantissa.toString(), latestBlockMined, liquidationIncentiveMantissa.toString(), latestBlockMined);
  stmt.run('PROTOCOL_oracle', oracleAddress, latestBlockMined, oracleAddress, latestBlockMined);
}

module.exports = { syncInternalValues };
