const _ = require('underscore');
const ethers = require('ethers');
const cERC20Abi = require('../abis/cERC20.json');
const { getDBConnection, GET_MISSING_BORROWS_INDEXES } = require('../utils');
const { URL, ARCHIVE } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);
const archiveProvider = new ethers.providers.JsonRpcProvider(ARCHIVE);

const db = getDBConnection();

const stmt = db.prepare(`
INSERT INTO CTOKENS_BORROW_OLDER_INDEXES
  (ctoken, borrow_index, ctoken_block_log_unique_id, block_number, log_index)
VALUES
  (?,?,?,?,?)
ON CONFLICT(ctoken_block_log_unique_id)
DO UPDATE SET borrow_index=excluded.borrow_index, block_number=excluded.block_number, log_index=excluded.log_index, ctoken_block_log_unique_id=excluded.ctoken_block_log_unique_id
`);

async function syncFast(cTokensAndBlocks, useArchive) {
  let borrowIndexesData = [];

  if (useArchive) {
    for (let i = 0; i < cTokensAndBlocks.length; i+=100) {
      let temp = await Promise.all(cTokensAndBlocks.slice(i, i+100).map(async ({ blockNumber, ctoken }) => {
        const ctoken_contract = new ethers.Contract(ctoken, cERC20Abi, useArchive ? archiveProvider : provider);
        const borrowIndex = await ctoken_contract.borrowIndex({ blockTag: blockNumber });
    
        return [borrowIndex, ctoken, blockNumber];
      }));

      borrowIndexesData = [...borrowIndexesData, ...temp];

      console.log(`syncBorrowsDerivedData:Archive -> Batch ${i} of ${cTokensAndBlocks.length}`);

      // Wait 2 secs before trying
      await new Promise(r => setTimeout(r, 2000));
    }
  } else {
    borrowIndexesData = await Promise.all(cTokensAndBlocks.map(async ({ blockNumber, ctoken }) => {
      const ctoken_contract = new ethers.Contract(ctoken, cERC20Abi, useArchive ? archiveProvider : provider);
      const borrowIndex = await ctoken_contract.borrowIndex({ blockTag: blockNumber });
  
      return [borrowIndex, ctoken, blockNumber];
    }));
  }

  db.transaction(() => {
    borrowIndexesData.forEach(([borrowIndex, ctoken, blockNumber]) => {
      stmt.run(
        ctoken,
        borrowIndex.toString(),
        ctoken + blockNumber + '0',
        blockNumber,
        '0'
      );
    });
  })();
}

async function syncBorrowsDerivedData() {
  const missingBorrowsSet = db.prepare(GET_MISSING_BORROWS_INDEXES).all();
  const blockNumbers = missingBorrowsSet.map((ret) => { return Number(ret.block_number) });
  const cTokensAndBlocks = missingBorrowsSet.map((ret) => ({ ctoken: ret.ctoken, blockNumber: ret.block_number }));
  const lastBlock = await provider.getBlockNumber();

  if (missingBorrowsSet.length === 0) return;

  const minBlock = _.min(blockNumbers);

  return syncFast(cTokensAndBlocks, (lastBlock-minBlock) > 120);
}

module.exports = { syncBorrowsDerivedData };
