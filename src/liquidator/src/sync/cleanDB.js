const { getDBConnection } = require('../utils');

function cleanDB() {
  const db = getDBConnection();
  const stmt = db.prepare(`DELETE FROM ACCOUNT_TRANSFERS`);
  const stmt1 = db.prepare(`DELETE FROM ACCOUNT_INTEREST_MINTABLE`);

  // Delete only those market events with a Match in ACCOUNT_BALANCES, We still need pending market entered
  const stmt2 = db.prepare(`
    DELETE FROM MARKET_EVENTS
    WHERE ID IN (
      SELECT me.ID FROM MARKET_EVENTS me
      INNER JOIN ACCOUNT_BALANCES ab
        ON (ab.ctoken = me.ctoken AND ab.account = me.account)
    );
  `);

  db.transaction(() => {
    stmt.run();
    stmt1.run();
    stmt2.run();
  })();
}

module.exports = { cleanDB }
