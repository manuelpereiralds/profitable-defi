const { PROTOCOL } = currentConfiguration;
const { getDBConnection } = require('../utils');

function syncAccountBalances() {
  const db = getDBConnection();
  const IS_CREAMFI = PROTOCOL === 'CREAMFI';
  const IS_VENUS = PROTOCOL === 'VENUS';

  const stmt = db.prepare(`
    INSERT INTO ACCOUNT_BALANCES
      (ctoken, account, balance, account_ctoken_unique_id, last_block_number, initial_block_number)
      SELECT
        ctoken,
        account,
        SUM(amount) as balance,
        account || ctoken as account_ctoken_unique_id,
        MAX(block_number) as last_block_number,
        MIN(block_number) as initial_block_number
      FROM ACCOUNT_TRANSFERS
      WHERE block_number > COALESCE((SELECT last_block_number FROM ACCOUNT_BALANCES ORDER BY last_block_number DESC LIMIT 1), -1)
      GROUP BY ctoken, account
    ON CONFLICT(account_ctoken_unique_id)
    DO UPDATE SET balance = add_big_number(balance, excluded.balance), last_block_number = excluded.last_block_number
  `);

  const stmt1 = db.prepare(`
    UPDATE ACCOUNT_BALANCES
    SET status = 1, market_entered_block_number = me.block_number
    FROM (SELECT * FROM MARKET_EVENTS WHERE event = 'MarketEntered') AS me
    WHERE ACCOUNT_BALANCES.ctoken = me.ctoken AND ACCOUNT_BALANCES.account = me.account;
  `);

  // comment check -> me.block_number > ACCOUNT_BALANCES.last_block_number
  const stmt2 = db.prepare(`
    UPDATE ACCOUNT_BALANCES
    SET status = 0
    FROM (SELECT * FROM MARKET_EVENTS WHERE event = 'MarketExited') AS me
    WHERE ACCOUNT_BALANCES.ctoken = me.ctoken AND ACCOUNT_BALANCES.account = me.account; 
  `);

  const stmt3 = db.prepare(`
      UPDATE ACCOUNT_BALANCES as AB
      SET BALANCE = CB.BALANCE, last_block_number = CB.block_number
      FROM COLLATERAL_CAP_TOKENS_ACCOUNTS as CB
      WHERE AB.CTOKEN = CB.CTOKEN AND AB.ACCOUNT = CB.ACCOUNT;
  `);

  // Fix CREAM.FINANCE BUG with users where accountCollateralTokens is not synced with accountTokens
  // const stmt6 = db.prepare(`
  //     UPDATE ACCOUNT_BALANCES
  //     SET balance = t2.new_balance
  //     FROM (
  //     SELECT ccta.account, ab.id, 0 as new_balance FROM ACCOUNT_BALANCES ab
  //     LEFT JOIN COLLATERAL_CAP_TOKENS_ACCOUNTS ccta
  //     ON (ab.ctoken = ccta.ctoken AND ccta.account = ab.account)
  //     INNER JOIN CTOKENS_COLLATERAL_FACTORS ccf ON (ab.ctoken = ccf.ctoken)
  //     WHERE ccta.block_number >= ab.last_block_number
  //     AND ccta.account IS NULL AND ccf.collateral_cap = 1
  //     ) AS t2
  //     WHERE ACCOUNT_BALANCES.id = t2.id;
  // `);

  const stmt7 = db.prepare(`
    INSERT INTO BORROWS
    (account, ctoken, account_borrows, account_ctoken_unique_id, block_number)
      SELECT
        account,
        LOWER(MAX(ctoken)) as ctoken,
        add_all(amount) as account_borrows,
        account || LOWER(ctoken) as account_ctoken_unique_id,
        MAX(block_number) as block_number
      FROM ACCOUNT_INTEREST_MINTABLE
      GROUP BY account
    ON CONFLICT(account_ctoken_unique_id)
    DO UPDATE SET account_borrows = add_big_number(account_borrows, excluded.account_borrows), block_number = excluded.block_number
  `);

  db.transaction(() => {
    stmt.run();
    stmt1.run();
    stmt2.run();
    if (IS_CREAMFI) stmt3.run();
    if (IS_VENUS) stmt7.run();
    // if (IS_CREAMFI) stmt6.run();
  })();
}

module.exports = { syncAccountBalances }
