const ethers = require('ethers');
const cERC20Abi = require('../abis/cERC20.json');
const oracleAbi = require('../abis/oracle.json');
const { getDBConnection, GET_CTOKENS_SQL_QUERY } = require('../utils');
const { URL, PROTOCOL } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

async function syncMarkets() {
  const db = getDBConnection();
  const cTokens = db.prepare(GET_CTOKENS_SQL_QUERY).all();

  let oracleAddress;

  if (PROTOCOL === "CREAMFI") {
    const { value: oracleProxyAddress } = db.prepare(`SELECT value FROM PROTOCOL_VALUES WHERE internal_name = 'oracle_proxy'`).get();
    oracleAddress = oracleProxyAddress;
  } else if (PROTOCOL === "VENUS") {
    const { value: oracleProtocolAddress } = db.prepare(`SELECT value FROM PROTOCOL_VALUES WHERE internal_name = 'PROTOCOL_oracle'`).get();
    oracleAddress = oracleProtocolAddress;
  }

  const oracleContract = new ethers.Contract(oracleAddress, oracleAbi, provider); 

  try {
    const latestBlockMined = await provider.getBlockNumber();
    console.log(`Going to sync ${cTokens.length} markets until block ${latestBlockMined}`);

    const updatedData = await Promise.all(cTokens.map(async ({ ctoken: cToken }) => {
      const ctoken = new ethers.Contract(cToken, cERC20Abi, provider);
      const [borrowIndex, exchangeRate, price] = await Promise.all(
        [ctoken.borrowIndex(), ctoken.exchangeRateStored(), oracleContract.getUnderlyingPrice(cToken)]
      );
      return [cToken, borrowIndex, exchangeRate, price, latestBlockMined];
    }));

    const stmt = db.prepare(`
      INSERT INTO CTOKENS_METADATA
        (ctoken, borrow_index, exchange_rate, underlying_price, block_number)
      VALUES
        (?,?,?,?,?)
      ON CONFLICT(ctoken)
      DO UPDATE SET exchange_rate=?,borrow_index=?, underlying_price=?, block_number=?
    `);
  
    db.transaction(() => {
      updatedData.forEach(([cToken, borrowIndex, exchangeRate, price, latestBlockMined]) => {
        stmt.run(cToken, borrowIndex.toString(), exchangeRate.toString(), price.toString(), latestBlockMined, exchangeRate.toString(), borrowIndex.toString(), price.toString(), latestBlockMined);
      });
    })();
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = { syncMarkets };
