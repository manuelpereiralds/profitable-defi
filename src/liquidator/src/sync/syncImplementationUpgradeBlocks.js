const ethers = require('ethers');
const { hexlify } = ethers.utils;
const { getDBConnection, GET_COLLATERAL_CAP_CTOKENS } = require('../utils');
const { URL } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

const NEW_IMPLEMENTATION_TOPIC = '0xd604de94d45953f9138079ec1b82d533cb2160c906d1076d1f7ed54befbca97a';

const synchronizeImplUpgradeBlocks = async () => {
  const db = getDBConnection();

  const cTokens = db.prepare(GET_COLLATERAL_CAP_CTOKENS).all();
  const cTokenAddresses = cTokens.map((cToken) => cToken.ctoken);

  const BLOCK_START = 12380167;
  const lastBlock = await provider.getBlockNumber();

  const stepSize = 5000;

  const stmt = db.prepare(`
    INSERT INTO CTOKENS_IMPLEMENTATION_UPGRADE_BLOCKS
      (ctoken, block_number)
    VALUES
      (?,?)
    ON CONFLICT(ctoken)
    DO NOTHING
  `);

  let fromBlock = BLOCK_START;
  let i = 0;
  let toBlock = 0;

  do {
    toBlock = fromBlock + stepSize;
    if (toBlock > lastBlock) {
      toBlock = lastBlock;
    }

    const pastEvents = await provider.send('eth_getLogs', [{
      address: cTokenAddresses,
      fromBlock: hexlify(fromBlock),
      toBlock: hexlify(toBlock),
      topics: [NEW_IMPLEMENTATION_TOPIC]
    }]);

    db.transaction(() => {
      for (pastEvent of pastEvents) {
        const blockNumber = Number(pastEvent.blockNumber);
        const cToken = pastEvent.address.toLowerCase();

        stmt.run(cToken, blockNumber);
      }
    })();

    fromBlock = toBlock;
    i++;
  } while (toBlock != lastBlock);
}

module.exports = { synchronizeImplUpgradeBlocks };
