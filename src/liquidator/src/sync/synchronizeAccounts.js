const ethers = require('ethers');
const { hexlify, hexStripZeros } = ethers.utils;
const cERC20Abi = require('../abis/cERC20.json');
const { getDBConnection, GET_CTOKENS_SQL_QUERY, waitForTransactions } = require('../utils');
const { URL } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

const BORROW_TOPIC_HASH = '0x13ed6866d4e1ee6da46f845c46d7e54120883d75c5ea9a2dacc1c4ca8984ab80';
const REPAY_TOPIC_HASH = '0x1a2a22cb034d26d1854bdc6666a5b91fe25efbbb5dcad3b0355478d6f5c362a1';
const TRANSFER_TOPIC_HASH = '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef';
const USER_COLLATERAL_CHANGED_TOPIC = '0x1d22042d9eb89f2620572acbf8d85b66fba5a2ca19d166d8659574440175c964';

const collateralCapBalanceChangedEvent = ["event UserCollateralChanged(address account, uint newCollateralTokens)"];
const iface = new ethers.utils.Interface(cERC20Abi.concat(collateralCapBalanceChangedEvent));

const synchronizeAccounts = async (
  latestBlockSynchronized,
  latestBlockMined
) => {
  const db = getDBConnection();

  const cTokens = db.prepare(GET_CTOKENS_SQL_QUERY).all();
  const cTokenAddresses = cTokens.map((cToken) => cToken.ctoken);

  const pastEvents = await provider.send('eth_getLogs', [{
    address: cTokenAddresses,
    fromBlock: hexStripZeros(hexlify(latestBlockSynchronized)),
    toBlock: hexStripZeros(hexlify(latestBlockMined)),
    topics: [[BORROW_TOPIC_HASH, REPAY_TOPIC_HASH, TRANSFER_TOPIC_HASH, USER_COLLATERAL_CHANGED_TOPIC]]
  }]);

  const borrowsSTMT = db.prepare(`
    INSERT INTO BORROWS
      (account, ctoken, account_borrows, account_ctoken_unique_id, block_number, transaction_hash, log_index)
    VALUES
      (?,?,?,?,?,?,?)
    ON CONFLICT(account_ctoken_unique_id)
    DO UPDATE SET account_borrows = ?, block_number = ?, transaction_hash = ?, log_index = ?
  `);

  const transfersSTMT = db.prepare(`
    INSERT INTO ACCOUNT_TRANSFERS
      (ctoken, account, amount, account_ctoken_hash_index_unique_id, block_number, transaction_hash, log_index)
    VALUES
      (?,?,?,?,?,?,?)
    ON CONFLICT(account_ctoken_hash_index_unique_id)
    DO NOTHING
  `);

  const collateralTokensSTMT = db.prepare(`
    INSERT INTO COLLATERAL_CAP_TOKENS_ACCOUNTS
      (ctoken, account, balance, account_ctoken_unique_id, block_number, transaction_hash, log_index)
    VALUES
      (?,?,?,?,?,?,?)
    ON CONFLICT(account_ctoken_unique_id)
    DO UPDATE SET balance = excluded.balance, block_number = excluded.block_number, transaction_hash = excluded.transaction_hash, log_index = excluded.log_index
  `);

  db.transaction(() => {
    for (pastEvent of pastEvents) {
      const { args, name: eventName } = iface.parseLog(pastEvent);
      const blockNumber = Number(pastEvent.blockNumber);
      const cToken = pastEvent.address.toLowerCase();

      switch (eventName) {
        case "Borrow": {
          const borrower = args[0];
          const amount = args[2].toString();
    
          borrowsSTMT.run(
            borrower,
            cToken,
            amount,
            borrower + cToken,
            blockNumber,
            pastEvent.transactionHash,
            pastEvent.logIndex,
            amount,
            blockNumber,
            pastEvent.transactionHash,
            pastEvent.logIndex
          );
          break;
        }
        case "RepayBorrow": {
          const borrower = args[1];
          const amount = args[3].toString();
    
          borrowsSTMT.run(
            borrower,
            cToken,
            amount,
            borrower + cToken,
            blockNumber,
            pastEvent.transactionHash,
            pastEvent.logIndex,
            amount,
            blockNumber,
            pastEvent.transactionHash,
            pastEvent.logIndex
          );
          break;
        }
        case "Transfer": {
          const fromAccount = args[0];
          const toAccount = args[1];
      
          const amount = args[2].toString();
      
          if (fromAccount.toLowerCase() !== cToken.toLowerCase()) {
            transfersSTMT.run(
              cToken,
              fromAccount,
              '-' + amount,
              fromAccount + cToken + pastEvent.transactionHash + pastEvent.logIndex,
              Number(pastEvent.blockNumber),
              pastEvent.transactionHash,
              pastEvent.logIndex
            );
          }
      
          if (toAccount.toLowerCase() !== cToken.toLowerCase()) {
            transfersSTMT.run(
              cToken,
              toAccount,
              amount,
              toAccount + cToken + pastEvent.transactionHash + pastEvent.logIndex,
              Number(pastEvent.blockNumber),
              pastEvent.transactionHash,
              pastEvent.logIndex
            );
          }
          break;
        }
        case "UserCollateralChanged": {
          const account = args[0];
          const balance = args[1].toString();
    
          collateralTokensSTMT.run(
            cToken,
            account,
            balance,
            account + cToken,
            blockNumber,
            pastEvent.transactionHash,
            pastEvent.logIndex
          );
          break;
        }
        default:
          break;
      }
    };
  })();

  await waitForTransactions();
}

module.exports = { synchronizeAccounts };
