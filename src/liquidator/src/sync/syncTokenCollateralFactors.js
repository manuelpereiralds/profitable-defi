const ethers = require('ethers');
const ComptrollerAbi = require('../abis/ComptrollerImpl.json');
const ComptrollerBaseAbi = require('../abis/Comptroller.json');
const { getDBConnection, GET_CTOKENS_SQL_QUERY } = require('../utils');
const { URL, COMPTROLLER, PROTOCOL } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

const VERSION_COLLATERAL_CAP = 1;

async function syncTokenCollateralFactors() {
  const db = getDBConnection();
  const abi = PROTOCOL === "CREAMFI" ? ComptrollerAbi : ComptrollerBaseAbi;

  const latestBlockMined = await provider.getBlockNumber();

  const comptroller = new ethers.Contract(COMPTROLLER, abi, provider);

  const cTokens = db.prepare(GET_CTOKENS_SQL_QUERY).all();

  const stmt = db.prepare(`
    INSERT INTO CTOKENS_COLLATERAL_FACTORS
      (ctoken, collateral_factor, collateral_cap, block_number)
    VALUES
      (?,?,?,?)
    ON CONFLICT(ctoken)
    DO UPDATE SET collateral_factor=?, block_number=?, collateral_cap=?
  `);

  const results = await Promise.all(cTokens.map(async (cToken) => {
    const [_, collateralFactorMantissa, isComped, version] = await comptroller.markets(cToken.ctoken);
    return [cToken.ctoken, collateralFactorMantissa.toString(), Number(version === VERSION_COLLATERAL_CAP)];
  }));

  db.transaction(() => {
    for (const [ctoken, collateralFactorMantissa, isCollateralCap] of results)
      stmt.run(ctoken, collateralFactorMantissa, isCollateralCap, latestBlockMined, collateralFactorMantissa, latestBlockMined, isCollateralCap);
  })();
}

module.exports = { syncTokenCollateralFactors };
