const ethers = require('ethers');
const { hexlify, hexStripZeros } = ethers.utils;
const cERC20Abi = require('../abis/cERC20.json');
const { getDBConnection, GET_CTOKENS_SQL_QUERY, waitForTransactions } = require('../utils');
const { URL } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

const LIQUIDATE_BORROW_TOPIC_HASH = '0x298637f684da70674f26509b10f07ec2fbc77a335ab1e7d6215a4b2484d8bb52';

const iface = new ethers.utils.Interface(cERC20Abi);

const db = getDBConnection();

const sync = async (
  cTokenAddresses,
  latestBlockSynchronized,
  latestBlockMined
) => {
  const pastEvents = await provider.send('eth_getLogs', [{
    address: cTokenAddresses,
    fromBlock: hexStripZeros(hexlify(latestBlockSynchronized)),
    toBlock: hexStripZeros(hexlify(latestBlockMined)),
    topics: [LIQUIDATE_BORROW_TOPIC_HASH]
  }]);

  const stmt = db.prepare(`
    INSERT INTO LIQUIDATIONS
      (ctoken, liquidator, borrower, repay_amount, ctoken_collateral, seize_tokens, block_number, transaction_hash, log_index)
    VALUES
      (?,?,?,?,?,?,?,?,?)
    ON CONFLICT(transaction_hash)
    DO NOTHING
  `);

  db.transaction(() => {
    for (pastEvent of pastEvents) {
      const { args } = iface.parseLog(pastEvent);
      const blockNumber = Number(pastEvent.blockNumber);
      const cToken = pastEvent.address.toLowerCase();

      const liquidator = args[0];
      const borrower = args[1];

      const repayAmount = args[2].toString();
      const cTokenCollateral = args[3];
      const seizeTokens = args[4].toString();

      stmt.run(
        cToken,
        liquidator,
        borrower,
        repayAmount,
        cTokenCollateral,
        seizeTokens,
        blockNumber,
        pastEvent.transactionHash,
        pastEvent.logIndex
      )    
    };
  })();

  await waitForTransactions();
}

async function syncLiquidations(iterations = 1) {
  const cTokens = db.prepare(GET_CTOKENS_SQL_QUERY).all();
  const cTokenAddresses = cTokens.map((cToken) => cToken.ctoken);

  const latestBlockMined = await provider.getBlockNumber();
  const stepSize = 5000;

  let fromBlock = 11705358;
  console.log(`fromBlock: ${fromBlock} -> latestBlockMined: ${latestBlockMined}`);

  let i = 0;
  let toBlock = 0;

  do {
    toBlock = fromBlock + stepSize;
    if (toBlock > latestBlockMined) {
      toBlock = latestBlockMined;
    }

    console.log(`Liquidations:Synchronizing ${fromBlock} -> ${toBlock}`);

    await Promise.all([
      sync(cTokenAddresses, fromBlock, toBlock),
    ]);

    fromBlock = toBlock;
    i++;
  } while (i < iterations && toBlock != latestBlockMined)

  console.log(`Total iterations done: ${i}`);
  console.log(`synchronization complete: ${toBlock}`);
}

module.exports = { syncLiquidations };
