const ethers = require('ethers');
const { hexlify, hexStripZeros } = ethers.utils;
const { getDBConnection, waitForTransactions } = require('../utils');
const { URL, INTEREST_TOKEN, INTEREST_COMPTROLLER } = currentConfiguration;

const provider = new ethers.providers.JsonRpcProvider(URL);

const TRANSFER_TOPIC_HASH = '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef';
const MINT_VAI_TOPIC = '0x002e68ab1600fc5e7290e2ceaa79e2f86b4dbaca84a48421e167e0b40409218a';
const REPAY_VAI_TOPIC = '0x1db858e6f7e1a0d5e92c10c6507d42b3dabfe0a4867fe90c5a14d9963662ef7e';
const REPAY_VAI_TOPIC2 = '0xd4a31707e3681a9b597321bfd1357488de9c7205d4195175b27e68b58a369d1e';
const MINT_VAI_FEE_TOPIC = '0xb0715a6d41a37c1b0672c22c09a31a0642c1fb3f9efa2d5fd5c6d2d891ee78c6';

const iface = new ethers.utils.Interface([
  "event MintVAI(address minter, uint mintVAIAmount)",
  "event RepayVAI(address payer, address borrower, uint repayVAIAmount)",
  "event RepayVAI(address repayer, uint repayVAIAmount)",
  "event MintFee(address minter, uint feeAmount)",
  "event Transfer(address indexed src, address indexed dst, uint wad)"
]);

const blockBase1 = 2471808; // NO MintVAI,RepayVAI EVENTS (JUST TRANSFER)
const BASE_BLOCK = 3204969; // MintVAI + RepayVAI EVENTS
const blockBase3 = 6730568; // MintFee + MintVAI + RepayVAI(NEW 3 args)

const syncMintableTokenEvents = async (
  latestBlockSynchronized,
  latestBlockMined
) => {
  const db = getDBConnection();

  const pastEvents = await provider.send('eth_getLogs', [{
    address: INTEREST_COMPTROLLER,
    fromBlock: hexStripZeros(hexlify(latestBlockSynchronized)),
    toBlock: hexStripZeros(hexlify(latestBlockMined)),
    topics: [[MINT_VAI_TOPIC, REPAY_VAI_TOPIC, REPAY_VAI_TOPIC2, MINT_VAI_FEE_TOPIC]]
  }]);

  const stmt = db.prepare(`
    INSERT INTO ACCOUNT_INTEREST_MINTABLE
      (ctoken, account, amount, account_index_unique_id, block_number, transaction_hash, log_index)
    VALUES
      (?,?,?,?,?,?,?)
    ON CONFLICT(account_index_unique_id)
    DO NOTHING
  `);

  db.transaction(() => {
    pastEvents.forEach((pastEvent) => {

      const { args, name: eventName } = iface.parseLog(pastEvent);
      const blockNumber = Number(pastEvent.blockNumber);
      const cToken = INTEREST_TOKEN.toLowerCase();

      const borrower = args[args.length-2];
      let amount = args[args.length-1].toString();

      if (eventName === "RepayVAI") {
        amount = "-" + amount;
      }

      stmt.run(
        cToken,
        borrower,
        amount,
        borrower + pastEvent.transactionHash + pastEvent.logIndex,
        blockNumber,
        pastEvent.transactionHash,
        pastEvent.logIndex
      );
    });
  })();

  await waitForTransactions();
}

const syncInterestStableTokenTransfers = async (
  latestBlockSynchronized,
  latestBlockMined
) => {
  const db = getDBConnection();

  const pastEvents = await provider.send('eth_getLogs', [{
    address: INTEREST_TOKEN,
    fromBlock: hexStripZeros(hexlify(latestBlockSynchronized)),
    toBlock: hexStripZeros(hexlify(latestBlockMined)),
    topics: [
      TRANSFER_TOPIC_HASH,
      [ethers.constants.HashZero, null],
      [null, ethers.constants.HashZero],
    ]
  }]);

  const stmt = db.prepare(`
    INSERT INTO ACCOUNT_INTEREST_MINTABLE
      (ctoken, account, amount, account_index_unique_id, block_number, transaction_hash, log_index)
    VALUES
      (?,?,?,?,?,?,?)
    ON CONFLICT(account_index_unique_id)
    DO NOTHING
  `);

  db.transaction(() => {
    pastEvents.forEach((pastEvent) => {
      const { args } = iface.parseLog(pastEvent);
      const { transactionHash, logIndex } = pastEvent;
      const blockNumber = Number(pastEvent.blockNumber);
      const cToken = INTEREST_TOKEN.toLowerCase();
      const fromAccount = args[0];
      const toAccount = args[1];
  
      const amount = args[2].toString();

      // we only take transfers from/to 0 address
      if (fromAccount.toLowerCase() !== ethers.constants.AddressZero
        && toAccount.toLowerCase() !== ethers.constants.AddressZero) {
        return;
      }

      if (fromAccount.toLowerCase() !== ethers.constants.AddressZero) {
        stmt.run(
          cToken.toLowerCase(),
          fromAccount,
          '-' + amount,
          fromAccount + transactionHash + logIndex,
          blockNumber,
          transactionHash,
          logIndex
        );
      }
  
      if (toAccount.toLowerCase() !== ethers.constants.AddressZero) {
        stmt.run(
          cToken.toLowerCase(),
          toAccount,
          amount,
          toAccount + transactionHash + logIndex,
          blockNumber,
          transactionHash,
          logIndex
        );
      }
    });
  })();

  await waitForTransactions();
}

async function syncInterestStableToken(fromBlock, toBlock) {
  if (fromBlock > BASE_BLOCK) {
    await syncMintableTokenEvents(fromBlock, toBlock);
  } else {
    await syncInterestStableTokenTransfers(fromBlock, toBlock);
  }
}

module.exports = { syncInterestStableToken };
