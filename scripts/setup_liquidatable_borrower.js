const { BigNumber } = require('bignumber.js');
const erc20Abi = require('../src/liquidator/src/abis/ERC20.json');
const cTokenAbi = require('../src/liquidator/src/abis/collateralCapCERC20.json');
const comptrollerAbi = require('../src/liquidator/src/abis/ComptrollerImpl.json');
const hre = require('hardhat');

const { ethers } = hre;

const addresses = {
  crBNB: "0x1ffe17b99b439be0afc831239ddecda2a790ff3a",
  crCREAM: "0x426d6d53187be3288fe37f214e3f6901d8145b62",
  WBNB: "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c",
  CREAM: "0xd4CB328A82bDf5f03eB737f37Fa6B370aef3e888",
  crWBNB: "0x15cc701370cb8ada2a2b6f4226ec5cf6aa93bc67",
  crBUSD: "0x2bc4eb013ddee29d37920938b96d353171289b7c",
  crRENBTC: "0x7f746a80506a4cafa39938f7c08ad59cfa6de418",
  renBTC: "0xfce146bf3146100cfe5db4129cf6c82b0ef4ad8c",
  BUSD: "0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56",
  CREAM_USER: "0xcdef3da8af54d3ca8729e353f161c8ea0e833c40",
  BUSD_USER: "0x8894E0a0c962CB723c1976a4421c95949bE2D4E3",
  renBTC_USER: "0xf977814e90da44bfa03b6295a0616a897441acec",
  COMPTROLLER: "0x589de0f0ccf905477646599bb3e5c622c84cc0ba",
}

async function transferFunds() {
  const signer = await ethers.getSigner(0);

  await (await signer.sendTransaction({ to: addresses.CREAM_USER, value: ethers.utils.parseEther('10') })).wait();
  await (await signer.sendTransaction({ to: addresses.renBTC_USER, value: ethers.utils.parseEther('10') })).wait();
  await (await signer.sendTransaction({ to: addresses.BUSD_USER, value: ethers.utils.parseEther('10') })).wait();
}

async function borrowBUSD() {
  const provider = ethers.provider;

  await provider.send("hardhat_impersonateAccount", [addresses.BUSD_USER]);
  const minter = await ethers.getSigner(addresses.BUSD_USER);

  // const crBNB = new ethers.Contract(addresses.crBNB, ["function mint() external payable"], bnbUSER)
  const BUSD = new ethers.Contract(addresses.BUSD, erc20Abi, minter)
  const crBUSD = new ethers.Contract(addresses.crBUSD, cTokenAbi, minter);

  console.log("BUSD_USER: ", (await BUSD.balanceOf(minter.address)).toString() / 1e8);

  const comptroller = new ethers.Contract(addresses.COMPTROLLER, comptrollerAbi, provider);
  const adminAddress = await comptroller.admin();

  await provider.send("hardhat_impersonateAccount", [adminAddress]);

  const admin = await ethers.getSigner(adminAddress);

  const newCollateralFactor = (0.9 * 1e18).toString();
  const initialCollateralFactor = "350000000000000000";

  await comptroller.connect(admin)._setCollateralFactor(addresses.crBUSD, newCollateralFactor);
  await comptroller.connect(minter).enterMarkets([addresses.crBUSD]);

  const mintAmount = new BigNumber(10000000000).times(1e18).toString(10);

  await BUSD.approve(crBUSD.address, mintAmount);
  await crBUSD.mint(mintAmount);

  const db = getDBConnection();
  const [
    { underlying_price: BUSD_PRICE },
  ] = db.prepare(
    `SELECT underlying_price FROM CTOKENS_METADATA cm where ctoken IN ('${addresses.crBUSD}')`
  ).all();

  console.log("BUSD_PRICE", BUSD_PRICE);

  const maxBorrowAmount = new BigNumber(890000).times(1e18).toString(10);
  await crBUSD.borrow(maxBorrowAmount);

  console.log("borrow balance:", (await crBUSD.borrowBalanceStored(minter.address)).toString());

  await comptroller.connect(admin)._setCollateralFactor(addresses.crBUSD, initialCollateralFactor);

  console.log(("current", await comptroller.getAccountLiquidity(minter.address)));
  // console.log(("current shortfall", await comptroller.getAccountLiquidity(minter.address))[2].toString() / 1e18);
}

async function borrowRenBTC() {
  const provider = ethers.provider;
  const signer = await ethers.getSigner(0);

  await transferFunds();

  await provider.send("hardhat_impersonateAccount", [addresses.CREAM_USER]);
  await provider.send("hardhat_impersonateAccount", [addresses.renBTC_USER]);
  const creamUSER = await ethers.getSigner(addresses.CREAM_USER);

  // const crBNB = new ethers.Contract(addresses.crBNB, ["function mint() external payable"], bnbUSER)
  const crCREAM = new ethers.Contract(addresses.crCREAM, cTokenAbi, creamUSER)
  const CREAM = new ethers.Contract(addresses.CREAM, erc20Abi, creamUSER)
  const renBTC = new ethers.Contract(addresses.renBTC, erc20Abi, creamUSER)
  const crRENBTC = new ethers.Contract(addresses.crRENBTC, cTokenAbi, creamUSER);

  console.log("renBTC_USER: ", (await renBTC.balanceOf(addresses.renBTC_USER)).toString() / 1e8);
  console.log("CREAM_USER: ", (await CREAM.balanceOf(addresses.CREAM_USER)).toString() / 1e18);

  const comptroller = new ethers.Contract(addresses.COMPTROLLER, comptrollerAbi, provider);

  const adminAddress = await comptroller.admin();

  await provider.send("hardhat_impersonateAccount", [adminAddress]);

  const admin = await ethers.getSigner(adminAddress);
  
  const newCollateralFactor = (0.9 * 1e18).toString();
  const initialCollateralFactor = "350000000000000000";

  await (await signer.sendTransaction({ to: adminAddress, value: ethers.utils.parseEther('10') })).wait();

  await comptroller.connect(admin)._setCollateralFactor(addresses.crCREAM, newCollateralFactor);

  const crCREAM_ADMIN = await crCREAM.admin();
  await provider.send("hardhat_impersonateAccount", [crCREAM_ADMIN]);

  await (await signer.sendTransaction({ to: crCREAM_ADMIN, value: ethers.utils.parseEther('10') })).wait();

  // Use a BIGGER AMOUNT of collateralCap
  const crAdmin = await ethers.getSigner(crCREAM_ADMIN);
  await crCREAM.connect(crAdmin)._setCollateralCap(new BigNumber(2).pow(255).toString(10));

  // Add some reserves of BUSD for borrowing amount to work.
  const renbtcUSER = await ethers.getSigner(addresses.renBTC_USER);
  const reserves = new BigNumber(60).times(1e8).toString(10);
  await renBTC.connect(renbtcUSER).approve(crRENBTC.address, reserves);
  await crRENBTC.connect(renbtcUSER)._addReserves(reserves);

  // Enter market
  await comptroller.connect(creamUSER).enterMarkets([addresses.crCREAM]);

  const mintAmount = new BigNumber(500).times(1e18).toString(10);

  await CREAM.approve(crCREAM.address, mintAmount);
  await crCREAM.mint(mintAmount);

  const maxBorrowAmount = (0.5*1e8).toString();

  await crRENBTC.borrow(maxBorrowAmount);

  console.log("borrow balance:", (await crRENBTC.borrowBalanceStored(creamUSER.address)).toString());

  await comptroller.connect(admin)._setCollateralFactor(addresses.crCREAM, initialCollateralFactor);

  console.log(("current", await comptroller.getAccountLiquidity(creamUSER.address)));
}

async function borrowWBNB() {
  const provider = ethers.provider;
  const signer = await ethers.getSigner(0);

  const crBNB = new ethers.Contract(addresses.crBNB, ["function mint() external payable"], signer);
  const WBNB = new ethers.Contract(addresses.WBNB, erc20Abi, signer)
  const crWBNB = new ethers.Contract(addresses.crWBNB, cTokenAbi, signer);

  const WBNB_USER = "0x59d779bed4db1e734d3fda3172d45bc3063ecd69";

  await provider.send("hardhat_impersonateAccount", [WBNB_USER]);
  const wbnbUser = await ethers.getSigner(WBNB_USER);

  await WBNB.connect(wbnbUser).approve(crWBNB.address, ethers.utils.parseEther('100'));
  await crWBNB.connect(wbnbUser)._addReserves(ethers.utils.parseEther('8.9'));

  const comptroller = new ethers.Contract(addresses.COMPTROLLER, comptrollerAbi, provider);
  const adminAddress = await comptroller.admin();

  await provider.send("hardhat_impersonateAccount", [adminAddress]);

  const admin = await ethers.getSigner(adminAddress);

  const newCollateralFactor = (0.9 * 1e18).toString();
  const initialCollateralFactor = "350000000000000000";

  await comptroller.connect(admin)._setCollateralFactor(addresses.crBNB, newCollateralFactor);
  await comptroller.connect(signer).enterMarkets([addresses.crBNB, addresses.crWBNB]);

  await crBNB.mint({ value: ethers.utils.parseEther("11") });
  await crWBNB.borrow(ethers.utils.parseEther("8.9"));

  console.log("borrow balance:", (await crWBNB.borrowBalanceStored(signer.address)).toString());
  console.log("borrow balance:", (await crWBNB.borrowBalanceCurrent(signer.address)).toString());

  await comptroller.connect(admin)._setCollateralFactor(addresses.crBNB, initialCollateralFactor);

  console.log(("current", await comptroller.getAccountLiquidity(signer.address)));
}

borrowRenBTC();
