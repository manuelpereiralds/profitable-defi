// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");
const { ethers } = require("hardhat");

async function main() {
  // We get the contract to deploy
  const creamFI_comptroller = "0x589DE0F0Ccf905477646599bb3E5C622C84cC0BA";
  const creamFI_CBNB = "0x1Ffe17B99b439bE0aFC831239dDECda2A790fF3A";
  const Liquidator = await hre.ethers.getContractFactory("Liquidator");
  const contract = await Liquidator.deploy(creamFI_comptroller, creamFI_CBNB);

  await contract.deployed();

  console.log("Liquidator deployed to:", contract.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
