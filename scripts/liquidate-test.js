const hre = require('hardhat');
const { BigNumber } = require("bignumber.js")

const { ethers } = hre;

async function run() {
  var provider = ethers.provider
  const Liquidator = await ethers.getContractFactory("Liquidator");
  const seizeUnderlying = "0x55d398326f99059ff775485246999027b3197955";
  const repayUnderlying = "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c";
  const seizeCToken = "0xfd5840cd36d94d7229439859c0112a4185bc0255";
  const repayCToken = "0xa07c5b74c9b40447a954e1466938b865b6bbea36";
  const account = "0x66ab6d9362d4f35596279692f0251db635165871";
  const CBNB = "0xa07c5b74c9b40447a954e1466938b865b6bbea36";
  const comptroller = "0xfD36E2c2a6789Db23113685031d7F16329158384";
  let liquidator = await Liquidator.deploy(comptroller, CBNB);
  liquidator = await liquidator.deployed();
  const closeFactorSafeSlippage = "000000000000000000";
  const liqIncent = "1100000000000000000";
  const closeFactor = "500000000000000000" - closeFactorSafeSlippage;
  const WBNB = "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c";
  const repayAmountBNB = "23870854660222235000";
  const seizedTokenAmount = new BigNumber(repayAmountBNB).times(liqIncent).dividedToIntegerBy().toString(10);

  await liquidator.liq_2627(account,repayCToken,seizeCToken,repayAmountBNB,ethers.constants.HashZero,ethers.constants.HashZero);
}

run();
