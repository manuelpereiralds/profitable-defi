require('dotenv').config({ path: '../env' });

global.currentConfiguration = require("../src/liquidator/src/config_cream.json");

const cTokenAbi = require('../src/liquidator/src/abis/collateralCapCERC20.json');
const { BigNumber } = require('bignumber.js');
const hre = require('hardhat');
const { getDBConnection } = require('../src/liquidator/src/utils');
const { getOneInchSwap, getAmountAfterPriceImpact } = require('../src/liquidator/src/functions');
const { MNEMONIC } = process.env;

const { ethers } = hre;

const wallet = ethers.Wallet.fromMnemonic(MNEMONIC);

BigNumber.config({ DECIMAL_PLACES: 0, ROUNDING_MODE: BigNumber.ROUND_DOWN });

async function run() {
  const provider = ethers.provider;
  const owner = wallet.connect(provider);
  const acc = await ethers.getSigner(0);

  await provider.send("hardhat_impersonateAccount", [owner.address]);

  const creamFI_comptroller = "0x589DE0F0Ccf905477646599bb3E5C622C84cC0BA";
  const creamFI_CBNB = "0x1Ffe17B99b439bE0aFC831239dDECda2A790fF3A";

  // const liquidator = await ethers.getContractAt("Liquidator", LIQUIDATOR_ADDRESS, owner);
  const Liquidator = await ethers.getContractFactory("Liquidator");
  let liquidator = await Liquidator.deploy(creamFI_comptroller, creamFI_CBNB);
  liquidator = await liquidator.deployed();

  console.log('Liquidator balance:', (await provider.getBalance(liquidator.address)).toString());
  console.log(`Liquidator at ${liquidator.address}`);

  const account = "0xcdef3da8af54d3ca8729e353f161c8ea0e833c40";
  const repayCToken = "0x7f746a80506a4cafa39938f7c08ad59cfa6de418";
  const repayUnderlying = "0xfce146bf3146100cfe5db4129cf6c82b0ef4ad8c";
  const seizeCToken = "0x426d6d53187be3288fe37f214e3f6901d8145b62";
  const seizeUnderlying = "0xd4CB328A82bDf5f03eB737f37Fa6B370aef3e888";

  const db = getDBConnection();
  const { 
    underlying_price: seizeUnderlyingPrice
  } = db.prepare(`SELECT underlying_price FROM CTOKENS_METADATA cm where ctoken = '${seizeCToken}'`).get();
  const { 
    underlying_price: repayUnderlyingPrice
  } = db.prepare(`SELECT underlying_price FROM CTOKENS_METADATA cm where ctoken = '${repayCToken}'`).get();

  console.log("seizeUnderlyingPrice", seizeUnderlyingPrice);
  console.log("repayUnderlyingPrice", repayUnderlyingPrice);

  const closeFactorSafeSlippage = "000000000000000000";

  const crRepayTokenContract = new ethers.Contract(repayCToken, cTokenAbi, provider);

  const closeFactor = "500000000000000000" - closeFactorSafeSlippage;
  const liqIncent = "1080000000000000000";
  const borrowBalance = (await crRepayTokenContract.borrowBalanceStored(account)).toString();

  console.log("borrowBalance", borrowBalance);

  const repayAmountETH = new BigNumber(repayUnderlyingPrice)
    .times(borrowBalance)
    .dividedToIntegerBy(1e18)
    .times(closeFactor)
    .dividedToIntegerBy(1e18)
    .toString(10);

  const seizedTokenAmount = new BigNumber(repayAmountETH)
    .times(liqIncent)
    .dividedToIntegerBy(seizeUnderlyingPrice)
    .toString(10);

  console.log("repayAmountETH", repayAmountETH);
  console.log("seizedTokenAmount", seizedTokenAmount);

  const WBNB = "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c";

  console.time('1inch api taken');

  const [txData0, fromTokenAmount0, toTokenAmount1, protocols0] = await getOneInchSwap(
    liquidator.address,
    WBNB,
    repayUnderlying,
    repayAmountETH
  );

  const [txData1, fromTokenAmount2, toTokenAmount3, protocols1] = await getOneInchSwap(
    liquidator.address,
    seizeUnderlying,
    WBNB,
    seizedTokenAmount
  );

  console.timeEnd('1inch api taken');

  console.log(fromTokenAmount0);
  console.log(toTokenAmount1);

  console.log(fromTokenAmount2);
  console.log(toTokenAmount3);

  const finalAmount = getAmountAfterPriceImpact(repayAmountETH, fromTokenAmount0, toTokenAmount3);

  console.log("calculated finalAmount:", finalAmount);

  console.log(JSON.stringify(protocols0));
  console.log("...")
  console.log(JSON.stringify(protocols1));

  await liquidator.liq_2627(
    account, // borrower
    repayCToken, // repayCToken
    seizeCToken, // collateral to seize
    finalAmount,
    txData0,
    txData1
  );

  console.log("Account balance:", (await provider.getBalance(acc.address)).toString() / 1e18);

  await liquidator.payoutMax(ethers.constants.AddressZero);

  console.log("Account balance:", (await provider.getBalance(acc.address)).toString() / 1e18);
}

run();
