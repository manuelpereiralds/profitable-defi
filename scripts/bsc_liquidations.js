const cTokenAbi = require('../src/liquidator/src/abis/collateralCapCERC20.json');
const { BigNumber } = require('bignumber.js');
const fetch = require('node-fetch');
const hre = require('hardhat');
const { getDBConnection } = require('../src/liquidator/src/utils');

const { ethers } = hre;

BigNumber.config({ DECIMAL_PLACES: 0, ROUNDING_MODE: BigNumber.ROUND_DOWN });

async function run() {
  const provider = ethers.provider;
  const acc = await ethers.getSigner(0);

  const Liquidator = await ethers.getContractFactory("LiquidatorBSC", acc);
  let liquidator = await Liquidator.deploy();
  liquidator = await liquidator.deployed();

  console.log('Liquidator balance:', (await provider.getBalance(liquidator.address)).toString());
  console.log(`Liquidator at ${liquidator.address}`);

  const account = "0xcdef3da8af54d3ca8729e353f161c8ea0e833c40";
  // const account = acc.address;
  const repayCToken = "0x7f746a80506a4cafa39938f7c08ad59cfa6de418";
  const seizeCToken = "0x426d6d53187be3288fe37f214e3f6901d8145b62";

  const db = getDBConnection();
  const { 
    underlying_price: seizeUnderlyingPrice
  } = db.prepare(`SELECT underlying_price FROM CTOKENS_METADATA cm where ctoken = '${seizeCToken}'`).get();
  const { 
    underlying_price: repayUnderlyingPrice
  } = db.prepare(`SELECT underlying_price FROM CTOKENS_METADATA cm where ctoken = '${repayCToken}'`).get();

  console.log("seizeUnderlyingPrice", seizeUnderlyingPrice);
  console.log("repayUnderlyingPrice", repayUnderlyingPrice);

  const pvData = db.prepare(`
    SELECT internal_name, value
    FROM PROTOCOL_VALUES WHERE internal_name
    IN ('close_factor_mantissa', 'liquidation_incentive_mantissa');
  `).all();

  const closeFactor = pvData.filter(pv => pv.internal_name === "close_factor_mantissa")[0].value;
  const liqIncent = pvData.filter(pv => pv.internal_name === "liquidation_incentive_mantissa")[0].value;

  console.log("closeFactor", closeFactor);
  console.log("liqIncent", liqIncent);

  const crRepayTokenContract = new ethers.Contract(repayCToken, cTokenAbi, provider);
  const borrowBalance = (await crRepayTokenContract.borrowBalanceStored(account)).toString();
  console.log("borrowBalance", borrowBalance);

  const repayAmountETH = new BigNumber(repayUnderlyingPrice)
    .times(borrowBalance)
    .dividedToIntegerBy(1e18)
    .times(closeFactor)
    .dividedToIntegerBy(1e18)
    .toString();

  const repayTokenAmount = new BigNumber(repayAmountETH)
    .times(1e18)
    .dividedToIntegerBy(repayUnderlyingPrice)
    .toString(10);

  const seizedTokenAmount = new BigNumber(repayAmountETH)
    .times(liqIncent)
    .dividedToIntegerBy(seizeUnderlyingPrice)
    .toString(10);

  console.log("repayAmountETH", repayAmountETH);
  console.log("repayTokenAmount", repayTokenAmount);
  console.log("seizedTokenAmount", seizedTokenAmount);

  const tx = await liquidator.liq_2627(
    account, // borrower
    repayCToken, // repayCToken
    seizeCToken, // collateral to seize
    repayTokenAmount,
    seizedTokenAmount,
  );

  // console.log(tx);
}

run();