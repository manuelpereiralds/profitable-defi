require('dotenv').config({ path: '../.env' });
const hre = require('hardhat');
const { ethers } = hre;
const { utils: bytes } = ethers;
const secp256k = require("ethereum-cryptography/secp256k1");
const { ecrecover, toBuffer, BN, pubToAddress, bufferToHex } = require("ethereumjs-util");
const chainLinkOracleAbi = require('../src/liquidator/src/abis/chainLinkOracle.json');
const accountsTestData = require('../accounts.json');

function getSignerFromSignature(hash, v, r, s) {
  const pub = ecrecover(toBuffer(hash), new BN(v + 27), toBuffer(r), toBuffer(s));
  const addrBuf = pubToAddress(pub);
  return bufferToHex(addrBuf);
}

async function setChainLinkOraclePrice(oracleAddress, newPrice) {
  const provider = ethers.provider;
  const accounts = await ethers.getSigners();
  const signers = accounts.slice(0, 16);
  const transmitters = accounts.slice(16);
  const oracle = new ethers.Contract(oracleAddress, chainLinkOracleAbi, provider);

  const ownerAddress = await oracle.owner();

  await provider.send("hardhat_impersonateAccount", [ownerAddress]);

  const ownerAccount = provider.getSigner(ownerAddress);

  console.log(`Found owner for oracle: ${await ownerAccount.getAddress()}`);

  const signer0 = signers[0];

  // Send some ETH to Oracle Owner account
  await signer0.sendTransaction({
    to: ownerAddress,
    value: bytes.parseEther('5.0'),
  });

  const transmitterAddresses = transmitters.map((tr) => tr.address);
  const signerAddresses = signers.map((sig) => sig.address);

  // Impersonate Oracle transmitters and signers.
  await oracle.connect(ownerAccount).setPayees(transmitterAddresses, transmitterAddresses);
  await oracle.connect(ownerAccount).setConfig(signerAddresses, transmitterAddresses, 5, 1, '0x00');

  // Retrieve last values
  const [
    configDigest, // bytes16 :  
    epoch, // uint32 :  
    round, // uint8 :  3
  ] = await oracle.latestTransmissionDetails();

  // rawReportContext consists of:
  // 11-byte zero padding = 88 bits
  // 16-byte configDigest
  // 4-byte epoch
  // 1-byte round
  const rawReportContext = bytes.hexConcat([
    bytes.hexZeroPad('0x', 11), // 11-byte zero padding
    bytes.hexDataSlice(configDigest, 0, 16), // 16-byte
    bytes.hexZeroPad(bytes.hexlify(epoch + 76), 4), // 4-byte
    bytes.hexZeroPad(bytes.hexlify(round + 1), 1), // 1-byte
  ]);

  // observer identities in bytes32 rawObservers
  const rawObservers = '0x040a0b0803060f05010907020c0e0d0000000000000000000000000000000000';

  const observations = [
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
    newPrice,
  ];

  const _report = bytes.defaultAbiCoder.encode(
    ["bytes32", "bytes32", "int192[]"],
    [rawReportContext, rawObservers, observations]
  );

  const payloadHash = bytes.keccak256(_report);

  const _rs = [];
  const _ss = [];
  const _rawVs = new Uint8Array(32);

  for (let i = 0; i < 10; i++) {
    const privateKey = bytes.arrayify('0x' + accountsTestData[i].privateKey);
    const { signature, recid: v } = secp256k.ecdsaSign(bytes.arrayify(payloadHash), privateKey);
    const r = signature.slice(0, 32);
    const s = signature.slice(32, 64);
    _rs.push(r);
    _ss.push(s);
    _rawVs[i] = v;
  }

  console.log(_rs.length);

  // const retrievedOwner = getSignerFromSignature(payloadHash, _rawVs[0], _rs[0], _ss[0]);

  // validate signature
  // if (retrievedOwner !== signers[0].address) {
    // throw new Error("Signatures mismatch");
  // }

  const transmitter0 = transmitters[0];

  // Transmit prices report to Chainlink OCR 
  const tx = await oracle.connect(transmitter0).transmit(
    _report,
    _rs,
    _ss,
    bytes.hexlify(_rawVs),
    { gasLimit: "0x6691b7" } // Force a GasLimit lower than MaxUint32 
  );
  await tx.wait();

  console.log("Price updated");
}

module.exports = {
  setChainLinkOraclePrice
}
