// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

// Import external Compound components
import "./interfaces/compound/ICERC20.sol";
import "./interfaces/compound/ICEther.sol";
import "./interfaces/venus/IVAIController.sol";

import "./interfaces/IWETH.sol";
import "./interfaces/IOneInchAggregatorV3.sol";

import "hardhat/console.sol";

contract LiquidationCallee {
    using SafeERC20 for IERC20;

    // Known addresses --------------------------------------------------------------------
    address public CETH;
    IVAIController public VAIcomptroller;
    address constant VAI = 0x4BD17003473389A42DAF6a0a729f6Fdb328BbBd7;
    address constant WETH = 0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c;
    address private constant ONE_INCH_AGGREGATOR_V3 = 0x11111112542D85B3EF69AE05771c2dCCff4fAa26;

    ICHI constant CHI = ICHI(0x0000000000004946c0e9F43F4Dee607b0eF1fA1c);

    // Boilerplate ------------------------------------------------------------------------
    receive() external payable {}

    function approve(address _token, address _spender, uint _amount) private {
        // 200 gas to read uint256
        if (IERC20(_token).allowance(address(this), _spender) < _amount) {
            // 20000 gas to write uint256 if changing from zero to non-zero
            // 5000  gas to write uint256 if changing from non-zero to non-zero
            IERC20(_token).safeApprove(_spender, type(uint256).max);
        }
    }

    function oneInchSwap(bytes memory _calldata, uint256 overrideAmount, uint256 overrideMinAmount) internal returns(uint, uint) {
        (address caller, SwapDescription memory desc, bytes memory _d) = abi.decode(_calldata, (address, SwapDescription, bytes));

        uint returnAmount;
        uint gasLeft;

        if (overrideMinAmount == 0) {
            overrideMinAmount = 1;
            // uint256 price = desc.amount > desc.minReturnAmount ? desc.amount / desc.minReturnAmount : desc.minReturnAmount / desc.amount;
            // overrideMinAmount = desc.amount > desc.minReturnAmount ? overrideAmount/price : overrideAmount*price;
        }

        SwapDescription memory desc1 = SwapDescription(
            desc.srcToken,
            desc.dstToken,
            desc.srcReceiver,
            desc.dstReceiver,
            overrideAmount,
            overrideMinAmount,
            desc.flags,
            desc.permit
        );

        console.log("desc1.minReturnAmount %s", desc1.minReturnAmount);
        console.log("desc.minReturnAmount %s", desc.minReturnAmount);

        (bool succ, bytes memory _cdata) = address(ONE_INCH_AGGREGATOR_V3).call(
            abi.encodeWithSelector(IOneInchAggregatorV3.swap.selector, caller, desc1, _d)
        );

        if (succ) {
            (returnAmount, gasLeft) = abi.decode(_cdata, (uint, uint));

            console.log("1inch returnAmount %s", returnAmount);
            console.log("desc1.minReturnAmount %s", desc1.minReturnAmount);

            if (returnAmount < desc1.minReturnAmount) {
                revert("Return amount is lower than minReturnAmount");
            }
        } else {
            if (_cdata.length < 68) {
                revert('Transaction reverted silently');
            }

            assembly {
                // Slice the sighash.
                _cdata := add(_cdata, 0x04)
            }

            string memory reason = abi.decode(_cdata, (string));
            revert(reason);
        }

        return (returnAmount, gasLeft);
    }

    /**
     * The function that gets called in the middle of a flash swap in order to liquidate an
     * account on Compound. This function assumes that the contract doesn't own and CTokens
     * before getting called.
     *
     * sender (address): the caller of `swap()`
     * @param amount0 (uint): the amount of token0 being borrowed
     * @param amount1 (uint): the amount of token1 being borrowed
     * @param data (bytes): data passed through from the caller
     */
    function pancakeCall(address /*sender*/, uint amount0, uint amount1, bytes calldata data) external {
        uint256 gasStart = gasleft();
        (
            address borrower,
            address repayCToken,
            address seizeCToken,
            uint repay,
            uint maxRepay,
            address owner,
            bytes memory _calldata0,
            bytes memory _calldata1
        ) = abi.decode(
            data,
            (
                 address,
                 address,
                 address,
                 uint,
                 uint,
                 address,
                 bytes,
                 bytes
            )
        );

        console.log("repayCToken: %s", repayCToken);
        console.log("d.repay: %s", repay);
        console.log("weth balance: %s", IERC20(WETH).balanceOf(address(this)));

        // Perform the liquidation
        if (repayCToken == CETH) {
            IWETH(WETH).withdraw(amount0 + amount1); // equivalent to max(amount0, amount1) since one is 0
            ICEther(CETH).liquidateBorrow{ value: repay }(borrower, seizeCToken);
        } else {
            address underlyingRepay = ICERC20Storage(repayCToken).underlying();
            uint256 repayAmount = repay;

            if (underlyingRepay != WETH) {
                IERC20(WETH).approve(ONE_INCH_AGGREGATOR_V3, repay);
                (uint256 returnAmount, uint256 gasLeft) = oneInchSwap(_calldata0, repay, 0);
                console.log("1Inch returnAmount %s", returnAmount);
                
                repayAmount = returnAmount > maxRepay ? maxRepay : returnAmount;
            }

            console.log("underlying asset: %s", underlyingRepay);
            console.log("new underlying balance: %s", IERC20(underlyingRepay).balanceOf(address(this)));
            console.log("Repaying only %s", repayAmount);

            approve(underlyingRepay, repayCToken, repayAmount);
            ICERC20(repayCToken).liquidateBorrow(borrower, repayAmount, seizeCToken);

            console.log("FINAL REPAY underlying balance: %s", IERC20(underlyingRepay).balanceOf(address(this)));
        }

        // Redeem cTokens for underlying ERC20 or ETH
        uint seized = ICERC20(seizeCToken).balanceOfUnderlying(address(this));
        ICERC20(seizeCToken).redeemUnderlying(seized);

        console.log("seized: %s", seized);

        uint256 debt = repay * 1002509028 / 1000000000; // Charge 0.2509027 % fee
        console.log("debt %s", debt);

        // Pay back pair
        if (seizeCToken == CETH) {
            IWETH(WETH).deposit{ value: debt }();
            IERC20(WETH).transfer(msg.sender, debt);
        } else {
            address underlyingSeized = ICERC20Storage(seizeCToken).underlying();
            console.log("d.seizeCToken.underlying(): %s", underlyingSeized);
            uint256 returnAmount = seized;

            if (underlyingSeized != WETH) {
                IERC20(underlyingSeized).approve(ONE_INCH_AGGREGATOR_V3, seized);
                (returnAmount,) = oneInchSwap(_calldata1, seized, debt);
                console.log("1Inch final returnAmount %s", returnAmount);
            }

            uint256 profit = returnAmount > debt ? returnAmount - debt : 0;

            console.log("ETH profit: %s", profit);
            console.log("FINAL SEIZE underlying balance: %s", IERC20(underlyingSeized).balanceOf(address(this)));

            IERC20(WETH).transfer(msg.sender, debt);

            IWETH(WETH).withdraw(profit);
        }

        uint256 gasSpent = 21000 + gasStart - gasleft() + (16 * msg.data.length);
        CHI.freeFromUpTo(owner, (gasSpent + 14154) / 41947);
    }
}
