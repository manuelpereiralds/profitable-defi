// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import './ICHI.sol';
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IGasDiscountExtension {
    function calculateGas(uint256 gasUsed, uint256 flags, uint256 calldataLength) external view returns (ICHI, uint256);
}

interface IAggregationExecutor is IGasDiscountExtension {
    function callBytes(bytes calldata data) external payable;  // 0xd9c45357
}

struct SwapDescription {
  IERC20 srcToken;
  IERC20 dstToken;
  address srcReceiver;
  address dstReceiver;
  uint256 amount;
  uint256 minReturnAmount;
  uint256 flags;
  bytes permit;
}

interface IOneInchAggregatorV3 {
  function swap(IAggregationExecutor caller,SwapDescription calldata desc,bytes calldata data) external payable returns (uint256 returnAmount, uint256 gasLeft);
}
