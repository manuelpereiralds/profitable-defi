// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IVAIController {
  function liquidateVAI(address borrower, uint repayAmount, address vTokenCollateral) external returns (uint, uint);
}
