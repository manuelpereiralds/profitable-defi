// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";

// Import Compound components
import "./interfaces/compound/ICERC20.sol";
import "./interfaces/compound/IComptroller.sol";

import "./interfaces/ICHI.sol";

import "./LiquidationCallee.sol";

import "hardhat/console.sol";

contract Liquidator is LiquidationCallee {
    using SafeERC20 for IERC20;

    address payable private owner;

    IComptroller public comptroller;
    uint private closeFact;

    constructor(address newComptroller, address baseETH) {
        owner = payable(msg.sender);
        _setComptroller(newComptroller);
        CETH = baseETH;
    }

    function setComptroller(address _comptrollerAddress) external {
        require(msg.sender == owner, "Not owner");
        _setComptroller(_comptrollerAddress);
    }

    function _setComptroller(address _comptrollerAddress) private {
        comptroller = IComptroller(_comptrollerAddress);
        closeFact = comptroller.closeFactorMantissa();
    }

    function payout(address _asset, uint _amount) public {
        if (_asset == address(0)) owner.transfer(_amount);
        else IERC20(_asset).transfer(owner, _amount);
    }

    function payoutMax(address _asset) public {
        if (_asset == address(0)) payout(_asset, address(this).balance);
        else payout(_asset, IERC20(_asset).balanceOf(address(this)));
    }

    /*
     * Liquidate a user with a flash swap and using 1inch.exchange as swap-route helper.
     *
     * @param _borrower (address): the user to liquidate
     * @param _repayCToken (address): a CToken for which the user is in debt
     * @param _seizeCToken (address): a CToken for which the user has a supply balance
     * @param _repayETH (uint): the amount (specified in units of ETHER) that can be repaid
     * @param _data0 (bytes): 1inch first swap details for repayToken
     * @param _data1 (bytes): 1inch second swap details for seizeToken
     * @return the mode
     */
    function liq_2627(
        address _borrower,
        address _repayCToken,
        address _seizeCToken,
        uint _repayETH,
        // uint _toMiner,
        bytes calldata _data0,
        bytes calldata _data1
    ) public {
        require(owner == msg.sender, "No way");
        uint256 gasStart = gasleft();

        // POOL with bigger liquidity
        address pair = 0x8FA59693458289914dB0097F5F366d771B7a7C3F; // PANCAKESWAP V2 Pair MBOX-WBNB
        uint maxRepay = ICERC20(_repayCToken).borrowBalanceStored(_borrower) * closeFact / 1e18;

        (, , uint shortfall) = comptroller.getAccountLiquidity(_borrower);

        require(shortfall != 0, "Not liquidatable anymore");

        bytes memory data = abi.encode(
            _borrower,
            _repayCToken,
            _seizeCToken,
            _repayETH,
            maxRepay,
            owner,
            _data0[4:],
            _data1[4:]
        );

        console.log("* maxRepay: %s", maxRepay);

        uint256 initialBalance = address(this).balance;

        if (IUniswapV2Pair(pair).token0() == WETH) {
            IUniswapV2Pair(pair).swap(_repayETH, 0, address(this), data);
        } else {
            IUniswapV2Pair(pair).swap(0, _repayETH, address(this), data);
        }

        uint256 newBalance = address(this).balance;

        console.log("Initial balance %s", initialBalance);
        console.log("Final balance %s", newBalance);

        uint256 gasSpent = 21000 + gasStart - gasleft() + (16 * msg.data.length);
        CHI.freeFromUpTo(owner, (gasSpent + 14154) / 41947);
    }
}
