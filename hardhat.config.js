require('dotenv').config();
require("@nomiclabs/hardhat-ethers");
require("@nomiclabs/hardhat-ganache");
const accounts = require('./accounts.json');

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async () => {
  const accounts = await ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.0",
  networks: {
    hardhat: {
      forking: {
        url: process.env.BSC_PROVIDER,
      },
      accounts: accounts,
      timeout: 1000000,
    },
    mainnet: {
      url: process.env.ETH_PROVIDER2,
      chainId: 1,
      accounts: {mnemonic: process.env.ETH_MNEMONIC}
    },
    bsc_mainnet: {
      url: process.env.BSC_PROVIDER,
      chainId: 56,
      accounts: {mnemonic: process.env.MNEMONIC}
    },
    ganache: {
      gasLimit: 6000000000,
      defaultBalanceEther: 10,
      fork: process.env.ETH_ARCHIVE_PROVIDER+"@13135500",
      hardfork: "muirGlacier",
      allowUnlimitedContractSize: true,
      totalAccounts: 32,
      mnemonic: "brownie",
      locked: false,
      url: 'http://localhost:8545',
      timeout: 100000,
    },
  },
  mocha: {
    timeout: 1000000
  }
};
