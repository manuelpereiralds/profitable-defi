require('dotenv').config({ path: '../.env' });
const { BigNumber } = require('bignumber.js');
const fetch = require('node-fetch');
const hre = require('hardhat');
const erc20Abi = require('../../src/liquidator/src/abis/ERC20.json');
const cTokenAbi = require('../../src/liquidator/src/abis/cERC20.json');
const bPoolAbi = require("../../src/liquidator/src/abis/BPOOL.json");

const _WETH = '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2';

const { ethers } = hre;

describe("Cream finance Liquidation Fork", function () {
  it.skip("Should liquidate", async function () {
    const provider = ethers.provider;

    // Use etherscan to find out some user with a lot of funds of the borrowed token.
    const userWithFundsAddress = "0xe93381fb4c4f14bda253907b18fad305d799241a";

    await provider.send("evm_unlockUnknownAccount", [userWithFundsAddress]);

    const fundedUser = await ethers.getSigner(userWithFundsAddress);

    const crCREAM = new ethers.Contract("0x892b14321a4fcba80669ae30bd0cd99a7ecf6ac0", cTokenAbi, fundedUser)
    const CREAM = new ethers.Contract("0x2ba592F78dB6436527729929AAf6c908497cB200", erc20Abi, fundedUser)
    const HUSD = new ethers.Contract("0xdF574c24545E5FfEcb9a659c229253D4111d87e1", erc20Abi, fundedUser)
    const crHUSD = new ethers.Contract("0xd692ac3245bb82319a31068d6b8412796ee85d2c", cTokenAbi, fundedUser);
    
    // Approve cToken to use funds from underlying
    await HUSD.approve(crHUSD.address, "2543000000000");

    // Test liquidation
    let tx = await crHUSD.liquidateBorrow(
      "0xF800d8407b1488BB6Dc3789c2D45147c25C38AF5", // borrower
      "2543000000000", // repayAmount in underlying
      "0x892b14321a4fcba80669ae30bd0cd99a7ecf6ac0" // collateral to seize
    );
    let receipt = await tx.wait();

    console.log(
      'Liquidator collateral balance',
      (await crCREAM.balanceOf(fundedUser.address)).toString()
    );

    const underlyingAmount = (await crCREAM.balanceOfUnderlying(fundedUser.address)).toString();

    console.log('Liquidator collateral Underlying balance:', underlyingAmount);

    tx = await crCREAM.redeemUnderlying(underlyingAmount);
    receipt = await tx.wait()

    const underlyingBalance = (await CREAM.balanceOf(fundedUser.address)).toString();

    console.log('Liquidator underlying balance:', underlyingBalance);

    // Swap using CreamSwap pool (Great for CREAM)
    const bpool = new ethers.Contract("0xa49b3c7c260ce8a7c665e20af8aa6e099a86cf8a", bPoolAbi, fundedUser);
    // const spotPrice = (await bpool.getSpotPrice(CREAM.address,_WETH)).toString();

    // Approve CreamSwap
    await CREAM.approve(bpool.address, underlyingBalance);

    tx = await bpool.swapExactAmountIn(
      CREAM.address,
      underlyingBalance,
      _WETH,
      0, // minimum amount out
      BigNumber(2).pow(255).toString(10) // max spot price we use 2^^256 MAX
    );
    
    receipt = await tx.wait();

    const WETH = new ethers.Contract(_WETH, erc20Abi, fundedUser);

    const WETH_BALANCE = (await WETH.balanceOf(fundedUser.address)).toString() / (10**18);

    console.log("WETH liquidator balance:", WETH_BALANCE);
  });
  it("Custom contract should liquidate", async function () {
    const provider = ethers.provider;
    const accounts = await ethers.getSigners();
    const Liquidator = await ethers.getContractFactory("Liquidator", accounts[0]);
    let liquidator = await Liquidator.deploy();
    liquidator = await liquidator.deployed();

    console.log('Liquidator balance:', (await provider.getBalance(liquidator.address)).toString());
    console.log(`Liquidator at ${liquidator.address}`);

    const desc0 = {
      tokenIn: "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
      tokenOut: "0xdF574c24545E5FfEcb9a659c229253D4111d87e1",
      amount: (7.5629029229519595*(1e18)).toString(),
      slippage: 5,
      chainId: 1,
    };

    const response = await fetch(
      `https://api.1inch.exchange/v3.0/${desc0.chainId}/swap`+
      `?fromTokenAddress=${desc0.tokenIn}&toTokenAddress=${desc0.tokenOut}`+
      `&amount=${desc0.amount}&fromAddress=${liquidator.address}&slippage=${desc0.slippage}`+
      `&destReceiver=${liquidator.address}&disableEstimate=true`
    );

    const swapJson = await response.json();

    const swap_data = swapJson.tx.data;
    const minimumOut = new BigNumber(swapJson.toTokenAmount).times(100-desc0.slippage).dividedToIntegerBy(100).toString();

    const tx = await liquidator.liquidateS(
      "0xF800d8407b1488BB6Dc3789c2D45147c25C38AF5", // borrower
      "0xd692ac3245bb82319a31068d6b8412796ee85d2c", // repayCToken
      "0x892b14321a4fcba80669ae30bd0cd99a7ecf6ac0", // collateral to seize
      0,
      swap_data,
    );

    console.log(tx);

    // assert.isTrue(tx.receipt.status);
    const events = tx.receipt.events;

    console.log(events);
  });
});
