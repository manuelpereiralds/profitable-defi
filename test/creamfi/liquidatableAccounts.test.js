require('dotenv').config({ path: '../.env' });
const { BigNumber } = require('bignumber.js');
const { ethers } = require('hardhat');
const { expect } = require('chai');
const _ = require('lodash');
const comptrollerAbi = require('../../src/liquidator/src/abis/ComptrollerImpl.json');
const oracleProxyAbi = require('../../src/liquidator/src/abis/oracle.json');
const { sync } = require("../../src/liquidator/src/synchronization");
const { getDBConnection, GET_ORACLES, GET_ORACLE_PROXY } = require('../../src/liquidator/src/utils');
const { getLiquidatableAccounts } = require('../../src/liquidator/src/getLiquidatableAccounts');
const { COMPTROLLER_ADDRESS } = process.env;

const provider = ethers.provider;

let oracleProxy;
const crUSDC = "0x44fbebd2f576670a6c33f6fc0b00aa8c5753b322";

const v1PriceOracleABI = [
  "function setPrice(address asset, uint requestedPriceMantissa)",
  "function getPrice(address asset) public view returns (uint)",
  "function _setPendingAnchor(address asset, uint newScaledPrice)",
  "function pendingAnchors(address asset) public view returns (uint)"
];

async function setProtocolOraclePrice(protocolAddress, underlying, cToken, percentageIncrease) {
  const adminAddress = '0x197939c1ca20c2b506d6811d8b6cdb3394471074'; // Admin allowed to force prices.
  const posterAddress = '0xd830a7413cb25fee57f8115cd64e565b0be466c3'; // Allowed to set PRICES.
  const oracle = new ethers.Contract(protocolAddress, v1PriceOracleABI, provider);

  console.log('Impersonating protocol oracle');

  await provider.send("evm_unlockUnknownAccount", [adminAddress, posterAddress] );
  const adminAccount = provider.getSigner(adminAddress);
  const posterAccount = provider.getSigner(posterAddress);

  const latestPrice = await oracleProxy.getUnderlyingPrice(cToken);

  const newPrice = new BigNumber(latestPrice.toString()).times(percentageIncrease).dividedToIntegerBy(100).toString(10);
  console.log(`Mock price: ${newPrice}`);

  // Post new price
  await oracle.connect(adminAccount)._setPendingAnchor(underlying, newPrice);
  await oracle.connect(posterAccount).setPrice(underlying, newPrice);

}

describe.skip("Cream finance Liquidatable accounts functions", function () {
  let comptroller;
  let db;

  beforeEach(async function () {
    db = getDBConnection();

    await sync(1000);
    console.log("Finished syncing Live DB with blockchain");

    comptroller = new ethers.Contract(COMPTROLLER_ADDRESS, comptrollerAbi, provider);
    const { value: oracleProxyAddress } = db.prepare(GET_ORACLE_PROXY).get();
    oracleProxy = new ethers.Contract(oracleProxyAddress, oracleProxyAbi, provider);
  });

  it("Should turn some borrower into liquidation", async function () {
    console.log(`Taking cToken: ${crUSDC} for testing.`);

    // Query current price and underlying address
    const { underlying_price: currentPrice, underlying_address: underlying, symbol } = db.prepare(`
      SELECT cm.underlying_price, ct.underlying_address, ct.symbol FROM CTOKENS_METADATA cm
      INNER JOIN CTOKENS ct ON (cm.ctoken = ct.ctoken)
      WHERE cm.ctoken = '${crUSDC}'
    `).get();

    console.log(`${symbol} underlying price ${currentPrice} for underlying ${underlying}`);

    // Query Oracles if it has a chainlink oracle.
    const oracles = db.prepare(GET_ORACLES).all();
    const { value: oracleAddress } = oracles.find((rs) => rs.internal_name === `${symbol}_oracle`) || {};
    const { value: protocolAddress } = oracles.find((rs) => rs.internal_name === 'PROTOCOL_oracle');

    // It has a ChainLink oracle.
    if (!!oracleAddress) {
      console.log(`${oracleAddress} oracle found for ${symbol}`);
      await setChainLinkOraclePrice(oracleAddress, 150);
    } else {
      // Just use Protocol oracle.
      console.log(`Using protocol oracle ${protocolAddress} for ${symbol}`);
      await setProtocolOraclePrice(protocolAddress, underlying, crUSDC, 150);
    }

    const newPrice = await oracleProxy.getUnderlyingPrice(crUSDC);

    // Temporary update DB with new price as well to match current blockchain state.
    console.log(`New ctoken underlying price set: ${newPrice}`);

    const accounts = getLiquidatableAccounts();

    expect(accounts).not.lengthOf(0, 'Still not liquidatable accounts after mocking price.');

    console.log(accounts[0]);

    // Try to liquidate

    // const [, prevLiquidity, prevShortfall] = await comptroller.getAccountLiquidity(testAccount.account);
    // console.log(`Previous liquidity for account was: ${prevLiquidity}`);

    // const [, currLiquidity, currShortfall] = await comptroller.getAccountLiquidity(testAccount.account);
    // console.log(`Current liquidity for account is: ${currLiquidity}`);

    // expect(prevShortfall.toString()).to.equal('0');
    // expect(currShortfall.toString()).not.equal('0');
  });
});
